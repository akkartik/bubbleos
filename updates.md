Changes in BubbleOS
===================

This is a reverse-chronological log by month of what’s changed.

March 2019
----------

Nothing yet.

February 2019
-------------

- Added somewhat flaky CPython and LuaJIT bindings for Yeso.  Now you
  can use Yeso to write graphical applications in C, Python (including
  Numpy support), and Lua, and run them in X11, on the Linux
  framebuffer console, or in theory under Wercam (though see below.)
- Various improvements to Yeso:
    - Made `yw_fds` Yeso interface just return an array.
    - Partly implemented X11 key state tracking, though it’s still not
      using KeymapNotify events.
    - Fixed an fd leak in image file reading.
    - Tested the Linux framebuffer console backend on more machines
      and fixed a bug on framebuffers with weird widths.
    - Add support for fully static linking and, in the other
      direction, for building Yeso backends as shared libraries.
- Fixed Toki to calculate your death expectancy properly on 32-bit
  machines.
- Added instructions for building on Nix (thanks, rain1!)
- Implemented Wercam simple windowing protocol (client and server).
  It’s still prototype-quality code, but Tetris, admu-shell, and
  Munching Squares can run under Wercam now.
- Reduced project build time to 7 seconds (on my laptop) with `make -j 32`.

In particular, I made no progress on Uncorp in February.

January 2019
------------

- Designed Wercam simple windowing protocol (Linux incarnation).
- Designed Uncorp portable assembly language, several times.
- Wrote prototype Uncorp interpreter in JS.
- Wrote Toki clock.
- Gave admu-shell really minimal UTF-8 support.
- Made admu-shell, Tetris, and Toki use the Cuerdas Caóticas font,
  adding some font handling functions to Yeso in the process.
- Overhauled Yeso documentation.
- Made Yeso build generate .a files (static libraries), making it easy
  to link with.
- Made Yeso interface considerably easier to use with `yp_p2`,
  `yp_line`, `yp_fill`, UTF-8 window titles, `yp_p_within`, and
  timeouts on `yw_wait`.
- Hacked together some image filter spikes.
- Fixed Yeso build on Arch Linux.
- Reorganized the directory tree.
- Added support for loading pixel fonts from files.
- Fix a hanging bug in admu-shell.
- Added visual bell to admu-shell.
- Optimized admu-shell.
- Added JPEG and PPM P6 reading support to Yeso.
- Added PPM P6 writing support to Yeso.
- Renamed xshμ to Yeso.
- Added documentation, uploaded to GitLab.

December 2018
-------------

- Wrote most of Yeso, under the name “xshmu” or “xshμ”, with Linux
  framebuffer console and X11 backends, and the first version of
  `vpng`, called `yv`.
- Scanned Cuerdas Caóticas font; wrote `makefont` to make a pixel font
  from the scan.
- Wrote oscilloscope, glyphed, Tetris, munching squares, and μpaint.
- Finished RPN calculator app.

October 2018
------------

- Wrote first version of “xshμ” (later “Yeso”).
- Wrote Wercaμ bouncing-ball demo; optimized alpha-blending with
  vectorization.
- Wrote admu terminal emulator.

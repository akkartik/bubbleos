BubbleOS: the only OS with a built-in death clock!
==================================================

> The staggering consumption of memory space by many widely used
> systems is due to violation of such fundamental rules of
> engineering. The requirement of many megabytes of store for an
> operating system is, albeit commonly tolerated, absurd and another
> hallmark of user-unfrie[n]dliness, or perhaps manufacturer
> friendliness. Its reason is none other than inadequate
> extensibility. —N. Wirth, _Project Oberon_, p. 15 (2005 edition)

BubbleOS is currently a collection of experimental components I’m
cobbling together into a complete, self-sufficient personal computing
operating system.  BubbleOS has a few somewhat conflicting objectives:

1. **Preserving human intellectual heritage**.  Current human
   computing systems have major compatibility problems even over the
   span of a few years — web pages that worked fine in Chrome 10 may
   not work at all in Chrome 40, and WWW pages are increasingly
   coupled to their origin servers, which are not very reliable.  As a
   consequence, many advances are lost, and in many ways the humans
   are intellectually running in place rather than progressing.  I
   want to explore how computing systems could be structured
   differently to support scholarship and progressive advance through
   reproducibility and censorship-resistance.  Maybe a “bubble” of
   protection can save some knowledge from the dark age the humans
   seem to be entering.

2. **Security**.  Information security, unlike the security of a safety
   deposit box or a military position, is binary — either you have it
   or you don’t, with respect to a particular threat model and
   security objective.  That is to say, either an attacker fitting
   your threat model has a way to violate your security objective, or
   they do not.  Right now, in practice, in almost all cases, they do.
   Although secure software is possible, current human computing
   systems are far too complex to be secure, and they are increasingly
   difficult to defend against Karger–Thompson attacks.  Maybe a
   small, simple, bootstrappable system can be a “bubble” of security
   in an insecure world.

3. **Practical software freedom**.  Right now, although (for example)
   Chromium, Firefox, Android, and Ubuntu are theoretically free
   software, in practice modifying them is out of reach of almost all
   humans, and essentially all humans outside their respective
   sponsoring organizations; they are only free software *de jure* and
   not *de facto*.  As a result, all four have at one or another time
   introduced changes that prejudiced the interests of the humans who
   use them, in order to provide some benefit to their sponsors, and
   the community of those users was not able to reject those changes.
   In large part, this is because they are very large and complex
   systems.  Maybe I can preserve a “bubble” of freedom in a world of
   mental enslavement.

4. **Hacker-friendliness**.  I want it to be as easy and fun to hack
   things on BubbleOS as I can figure out how to make it — as easy as
   an Apple \]\[, but with the power of modern hardware.  Most modern
   computing systems are frustratingly professional and bureaucratic,
   and it can consequently be a huge hassle to do even the simplest
   things; so they don’t get done unless someone is paying for them.
   Maybe I can create a “bubble” of fun in a world of drudgery, thus
   enabling humans to create wonderful things.

5. **Efficiency**.  Modern human computing systems are slower than much
   older computing systems despite running on much faster hardware,
   and this impedes their flexibility — if you need ten billion CPU
   cycles and a gigabyte of RAM just to start your calculator app, you
   probably aren’t going to be able to run it on an actual calculator.
   As a consequence, computing systems are frustrating to use and put
   only a tiny fraction of the hardware’s computing power in the hands
   of the humans.  Maybe I can create a "bubble" of efficiency and
   power in a world of waste and weakness.

6. **Innovation**.  I have a number of innovations to introduce that are
   difficult to fit into existing systems, and BubbleOS ought to be an
   interesting testbed for them.  Maybe I can create a “bubble” of
   innovation in a world of mindless imitation.

7. **Beauty**.  Things should be beautiful, not ugly.  I am not conceited
   enough to claim that what I have created is “beautiful”, but
   BubbleOS is designed to *allow the creation of* beauty, rather than
   frustrating it; and I do try to make BubbleOS itself beautiful.
   The degree to which I have succeeded is for readers to judge, not
   me; but I aspire for BubbleOS to be a “bubble” of beauty in a world
   that contains much ugliness.

8. **Education**.  Like Smalltalk, TECS, Minix, TempleOS, FORTH, and
   STEPS, BubbleOS is a system sufficiently small and simple that
   almost any human can understand all of it, if they take the time to
   do so.  Hopefully this will serve to demystify a number of topics
   many humans incorrectly think are impossible for them to
   understand.  Perhaps BubbleOS can be a “bubble” of
   comprehensibility and learning in a world that seems increasingly
   inexplicable and wilfully ignorant.

9. **Practicality**.  Like popular computing systems, BubbleOS is intended
   to be practically useful, not just a research prototype.

BubbleOS is structured as a set of somewhat independent components, so
that to a large extent you can use one part of it without using other
parts.  This is partly necessary for the bootstrapping process — if
the only way to build BubbleOS was by using a working BubbleOS
installation, it would be difficult to have confidence you were safe
from Karger–Thompson attacks, in part because David A. Wheeler’s
“diverse double-compiling” defense would be impossible in practice.
Also, though, it provides an incremental adoption path for BubbleOS,
avoiding the incompatibility roadblocks that has been such a
showstopper for the adoption of Smalltalk and TempleOS.

Current status
--------------

The BubbleOS components currently run on Linux and, reportedly, on
MacOS X.  They currently consist of the following:

- A simple GUI system called
  [Yeso][0]; it allows you to write graphical applications in a
  few lines of C, Python 2 or 3 (including PyPy), or Lua 5.1 (with LuaJIT),
  and you can then run them under X-Windows
  (locally, using shared memory); badly, on the Linux framebuffer
  console; or under Wercam, explained below.
- A reasonably efficient local-machine GUI IPC protocol called
  [Wercam][1] which allows programs to connect to a GUI server over a
  local socket, send it graphics to draw, and get back keyboard and
  mouse events.  There is a Yeso backend for Wercam, so Yeso programs
  can run under Wercam simply by linking to it.  It’s about 4 KiB of
  code and depends only on Yeso and POSIX.  The Wercam server is
  written on Yeso but is still somewhat incomplete.
- An ADM-3A terminal emulator called admu, which works well enough to
  run vim, screen, or tmux in.  It displays using Yeso, but the
  terminal emulation component is separable from Yeso, and is under
  600 bytes of machine code — under 400 bytes on ARM with Thumb2.
  Because the ADM-3A is supported by terminfo and termcap, most Unix
  terminal handling works on it, except for color.
- A Tetris game on Yeso.
- An image file viewer on Yeso.
- A clock, counting down the time until your probable death.
- An RPN calculator on Yeso or the terminal.
- Three non-antialiased pixel fonts: an ASCII font copied from
  X-Windows, a numeric font drawn using a janky pixel-font editor that
  I wrote in Yeso in a few minutes, and an ASCII font I drew years ago
  in the GIMP.
- A panoply of programming experiments that are not yet sufficiently
  packaged to be useful.

[0]: yeso/README.md
[1]: wercam/README.md

The image viewer, `yv`, is a very promising start to the objectives I
laid out at the top.  It views JPEGs, PNGs, and some PPM files, and
is several times faster than all the other PNG
viewers I have installed; on the framebuffer console it *was* running in
4 milliseconds, and it’s only 121 lines of C, compiling to
23 kilobytes stripped with X-Windows.
(Somewhere along the line I screwed it up and now it’s taking 36–100 ms.)

Uncorp, the low-level language, has a limited interpreter written in
Node.js.  It can currently calculate Fibonacci numbers.

Building
--------

Type `make`.

### Prerequisites ###

You need to have libpng, libjpeg, libbsd, Xlib, libXext, a C compiler, Make,
and node.js installed.  On Debian-derived systems I think you can do
this as follows:

    sudo apt install libjpeg-dev libpng-dev libx11-dev libxext-dev libbsd-dev gcc make nodejs-legacy

On Nix:

    nix-shell -p autoreconfHook -p xorg.libX11 -p xorg.libXext -p libpng -p libjpeg -p libbsd -p nodejs

Immediate plans (say, March 2019)
------------------------------------

I have a long list of things I *want* to do, but here are the ones I
think I can get done in March at a stretch:

- A portable untyped textual assembly language called [Uncorp][], designed
  primarily as a backend for compilers for high-level languages, with
  amd64 and AVR backends, generating object files that can link with
  standard C object files.
- Finishing Wercam, a very simple windowing system; the current
  prototype doesn’t yet handle multiple concurrent clients.
- Leconscrip: a memory-safe low-level statically-typed imperative language without
  garbage collection and with Lisp syntax, compiled to Uncorp,
  supporting a few features that are grievously missing in C, such as:
    - block arguments (for resource management, iteration, and
      user-defined control structures),
    - ML-style pattern-matching,
    - list comprehensions,
    - parametrically polymorphic hash tables,
    - multiple return values, and
    - struct literals.  (I guess those are in C99!)

[Uncorp]: uncorp/README.md

Short-term plans (2019)
-----------------------

This is even more ambitious, particularly considering that I want to
keep the whole system in the 20kloc (STEPS) to 100kloc (TempleOS)
range to keep it comprehensible.

- Some kind of shell capable of launching other programs and running
  in a terminal or in Wercam.
- Fixing a bunch of the other existing deficiencies in Yeso.
- Some way to use at least Unifont and ideally Noto for characters we
  don’t have glyphs for yet.
- Finishing the Yeso bindings for Python and LuaJIT.
- Yeso bindings in C++, Ruby, and maybe Node.JS.
- Yeso backends for VNC, X11 without shared memory (so it can run over
  the network), and Windows GDI.
- Cut and paste of text and images in Yeso.
- An immediate-mode GUI library for full-featured GUI applications,
  usable in Uncorp, Leconscrip, or C, including
- a text-editor widget.
- A more comprehensive 2D graphics library, including SSE-accelerated
  alpha-blending, reading and writing of some common image file
  formats, and an optimization-based layout system.
- Some more Yeso applications, including
    - a fractal explorer,
    - a mailreader,
    - a usable paint program, and
    - some more games.
- Support for mouse and keyup events on the Linux framebuffer console
  in Yeso.
- Major aesthetic improvements.
- Scrollback in admu.
- Maybe switching the Yeso X11 backend over from Xlib to XCB.
- Veskeno, a very simple deterministic integer virtual machine to
  replace or complement Uncorp.
- Emulators for other hardware built on Veskeno and Yeso.
- Uncorp and/or Veskeno backends for i386, armel, armeb, aarch64, and
  maybe WebAssembly, Vulkan, Intel GEN, or Radeon.
- A notebook interface for native-code or Veskeno graphical
  applications.
- An efficient PEG-parsing engine.
- A C-like syntax for Leconscrip using that parsing engine.
- A garbage-collected high-level language.
- A miniKANREN dialect.
- The ability to checkpoint and migrate running applications or a
  whole virtual machine image.
- A CRDT-based facility modeled on Secure Scuttlebutt and Redux for
  transparently distributed applications within a trust domain.
- A generic computation-caching system for computations over 100 μs in
  length.
- Dirness, a fast, secure, crashproof content-addressable filesystem
  with transaction support.
- Theorem provers capable of proving useful properties of programs.
- General-purpose mathematical optimization systems.
- A Hypothesis-style generative testing framework.
- Minimal TCP/IP support.
- HTTP clients and servers.
- An ssh client and server.
- Rera, a Bitcoin client.
- Making Wercam guarantee hard-real-time responsiveness.
- Intranin, a hard-real-time microkernel.
- Running self-hosted in QEMU.
- Minimal implementations of Emacs and Git.
- Self-hosting all BubbleOS development.
- A hypertext browser, though surely not all of HTML5.
- Downloadable archives of Wikipedia, Stack Exchange, OpenStreetMap,
  Project Gutenberg, and similar repositories of knowledge.
- A Nix-style systemwide configuration management system.
- Image-file codecs written in a BubbleOS language to eliminate the
  dependency on libpng.  (The Xlib or XCB dependency is fine, because
  presumably if you can get an X server, you can get Xlib and probably
  XCB too.)
- An adequate SQL RDBMS.
- A fast key-value store similar to LevelDB.
- A fast publish-subscribe system similar to ØMQ.

Long-term plans (2020–2320)
---------------------------

No.

License and Nonwarranty
-----------------------

Copyright Ⓒ 2018–2019 Kragen Javier Sitaker.

The BubbleOS system as a whole is free software; you can redistribute
it and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.  A copy of the license
is included in the file `LICENSE.txt`.

This system is distributed in the hope that it will be useful, but
**WITHOUT ANY WARRANTY**; without even the implied warranty of
**MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
GNU General Public License for more details.

Some parts of the repository are licensed under more liberal licenses,
as follows.

Cuerdas Caóticas, the font in yeso/cuerdas-caoticas-1.jpeg and
yeso/letrachica.jpeg, is Ⓒ 2019 Mina Tocci and is licensed under the
Creative Commons Attribution-ShareAlike 3.0 license.  Parts of
yeso/yeso-xlib.c are licensed under the liberal X11-like PicoLisp
license.

The rest of the code and text in BubbleOS is licensed only under the
GNU GPL version 3.

Acknowledgments
---------------

BubbleOS has benefited enormously from discussions, testing, and
contributions of art, ideas, and code by Kartik Agaram, Darius Bacon, John
Cowan, Charles Goodier, Dave Long, Lauri Love, Nick P.,
Sean B. Palmer, rain1, and Mina Tocci.

And, of course, it draws from the work and ideas of thousands of other
people; among the strongest influences not mentioned above, sorted
according to random numbers, are Doug Engelbart, Panini, Norm Hardy,
Bjarne Stroustrup, John Stuart Mill, Bret Victor, Tony Hoare, Lao Tse,
Chuck Moore, Rrrola, Niklaus Wirth, Lee Felsenstein, Tom Duff, Noam
Chomsky, Jonathan Shapiro, Alan Kay, Plato, Pekka Väänänen and the
rest of Macau Exports, Mark VandeWettering, Harold Abelson, viznut,
Warren Teitelman, Raph Levien, Barbara Liskov, Farbrausch, Omar
Cornut, Abdulaziz Ghuloum, Richard Stallman, L. Peter Deutsch, Richard
W. M. Jones, Rob Pike, Dan Bernstein, Stewart Brand, Emma Goldman,
Eric Tiedemann, Larry Wall, Iñigo Quilez, Mark S. Miller, Dan Ingalls,
Dan Luu, Tony Finch, Tom 7, René Descartes, Aaron Swartz, John
Gilmore, Ka-Ping Yee, Future Crew, Alan Turing, Matz, Guy Steele, and
Ken Thompson.

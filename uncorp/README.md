Uncorp: a universal compiler-oriented reverse Polish
====================================================

Uncorp is designed to be the lightest-weight viable solution to
getting a new language to run on many different CPUs and operating
systems (“targets”).  As I see the situation, the current viable
solutions to this multitarget problem are:

1. Compile to C.
2. Compile to a custom bytecode and write a bytecode interpreter on an
   existing multitarget system, such as C.
3. Compile to LLVM.
4. Write one backend for your compiler for each target.

The idea of Uncorp is to add a new option 5, “Compile to Uncorp,” and
to make Uncorp as compact as possible while remaining sufficiently
efficient, flexible, and debuggable to be a usable alternative except
for very demanding applications.

More polemically, Uncorp is my attempt to free BubbleOS from the
tyranny of the ANSI C standard, undefined behavior, and monstrous C
compilers that can’t even be compiled without a working C++
compiler — but without giving up interoperability with existing C
code.

Uncorp is not Forth
-------------------

I didn’t call it UINF, but maybe I should have.  Uncorp draws a great
deal from Forth, but if you are expecting a Forth, you are going to be
very disappointed.  Uncorp’s compile-time computation is somewhat
limited, with the intent of enabling the compiler to achieve
reasonable optimization levels, and in particular you can’t define new
compile-time functions or call your run-time functions from the
compiler (which may, after all, be running on a completely different
architecture).  Uncorp only has compile-time computation to make it
possible to generate code that’s portable across machines of different
word sizes and endiannesses.  To get metaprogramming, write a compiler
that outputs Uncorp code.

Uncorp language design
----------------------

Uncorp is a textual stack-machine language, like Forth or PostScript,
at more or less the level of assembly language.  It does not guarantee
the same results on all targets, but with some care you can produce
programs or subroutines in Uncorp that will run identically on many
different targets.  Like Forth or most assembly languages, it does not
have a static or dynamic type system, except in a trivial sense; it
provides access to raw memory, so your programs can do things like
determine the target byte order at runtime or perform an unaligned
access.  It does not provide dynamic memory management except in the
sense that it provides a framed call stack, but because it provides
raw memory pointers, programs in it can invoke external memory
management functions.

Unlike LLVM, Uncorp can start up and generate unoptimized code
extremely rapidly; it requires about 100 μs to start up plus 50 ns per
generated machine instruction, making it a practical alternative to
GNU Lightning despite its textual input.  On C targets, it produces
object files that can be seamlessly linked into C programs.

Unlike LLVM and C, Uncorp does not have undefined behavior; it merely
has target-dependent behavior.  Unlike C, Uncorp does not have
implicit coercions, because it does not have a type system.

The idea is that target-independent optimizations will be performed by
the compiler that generates the Uncorp code, not by Uncorp itself.
This includes things like constant folding, dead-code elimination,
function inlining, strength reduction, induction variable elimination,
loop unrolling, common subexpression elimination, tail-call
elimination, and dead-store elimination.  So Uncorp only has to do
relatively simple target-dependent optimizations, and the language is
designed such that even without any such optimizations, the resulting
code should run reasonably fast.

Each function in Uncorp has its own private operand stack, which is
separate from the call stack.  The call stack contains return
addresses and stack frames.

Syntactically, Uncorp consists of quoted strings and words separated
by spaces.

Uncorp has about 55 operations, depending on how you count, most of
which are the usual common CPU operations like scalar register-size
bitwise AND, scalar register-size multiplication, or memory fetches
and stores of different sizes.  Less common CPU operations such as
counting leading zeroes, population count, bitwise rotation,
double-length-result multiplication, and vector operations are not
included.

### Basic model ###

Uncorp has a compile-time operand stack, and a compile-time call
stack.  Uncorp output ostensibly has a run-time operand stack and a
run-time call stack, but on most targets the operand stack is more
theoretical than real.

Uncorp compiles by reading operations and appending bytes to two
spaces, code space and data space, corresponding roughly to Unix’s
`.text` and `.data` segments.  It can do arbitrary compile-time
computation in the process.  Then it writes out an object file which
is probably going to be processed by a linker of some kind, depending
on the target.  The code it emits goes through some optimization, and
in particular it tries to assign the run-time operand stack to
registers or constant-fold it away entirely.

### Four ways to do compile-time computation and control flow ###

It’s common in Uncorp programs for various quantities to depend on the
machine word size and other target attributes.  To support this,
Uncorp can do a great deal of compile-time computation, though it is
not very efficient.  It has compile-time definitions available for
most of its operations, and the operations `[` and `]` switch between
interpretation mode (for compile-time computation) and compilation
mode (for run-time computation).  Specifically, `[` switches from
compilation to interpretation, and `]` vice versa.  The conditional
structure `plic ? ploc : pluc ;` discussed later works in
compile-time code, so this also permits
conditional compilation with the sequences `? ]`, `[ : ]`, and `[ ;`.

There are some other operations related to compile-time computation:

- `.` pushes the current compilation address (the place where the next
  bit of code compiled will go) onto the compile-time operand stack.
- `#` takes the top item on the compile-time operand stack and adds
  code to code space to put it onto the run-time operand stack.
- `!` and `c!` in interpretation mode can be used to mutate
  already-emitted code or data, and `@` and `c@` can be used to read
  them.  For this purpose, referring to code and data labels at
  compile time will produce different results than at run time.  For
  example, on an AVR target, perhaps the code and data address spaces
  overlap; and perhaps the linking process after Uncorp is done will
  offset all code and data addresses by amounts that are unknown at
  compile time.
- `[ 3 equ plic ]` defines `plic` as a name which is equivalent to `3`;
  that is, using `plic` in compilation mode will append code to the
  code space that pushes the number 3 on the run-time operand stack,
  and using it in interpretation mode will append 3 to the
  compile-time operand stack.  This kind of thing is useful for saving
  the results of compile-time computations for later repeated use.

Compile-time memory access is bounds-checked so that you can only
access addresses within the already-emitted code and data spaces, so
you can’t segfault Uncorp with a compile-time computation unless
Uncorp is buggy.

Unlike with traditional assemblers, you cannot use the values of
labels before you have defined them, except in certain very restricted
ways.  In particular, you can push their values on the run-time
operand stack as long as you’ve declared them with `use`.

Compile-time computation can interfere badly with optimization;
whenever you switch into interpretation mode, Uncorp must guarantee
that the code so far emitted is complete and consistent.

### Twenty-four primitive computations ###

The most ordinary operations are:

- `+` and `-` perform pointer-sized wraparound integer addition and
  subtraction.
- `s/%` and `s*` perform pointer-sized wraparound signed division and
  modulus, and multiplication, respectively.
- `u/%` and `u*` perform pointer-sized wraparound *unsigned* division
  and modulus, and multiplication, respectively.
- `+.` `-.` `/%.` `*.` perform the corresponding floating-point
  operations.  Floating-point numbers are stored in the same operand
  stack as integers, but on machines with small pointers, may use more
  slots.
- `int_of_float` and `float_of_int` convert the top stack item between
  a floating-point number and an integer.
- `&` `|` `^` `~` perform pointer-sized bitwise AND, OR, XOR, and
  NOT, respectively.
- `<<` and `>>` perform arithmetic shifts on pointer-sized signed
  integers; `3000 4 >>` produces 187, and `-3000 4 >>` produces -188.
- `>>>` performs a logical right shift on pointer-sized unsigned
  integers.
- `@` and `!` fetch and store a pointer-sized quantity at the address
  on top of stack, respectively.  `c@` and `c!` fetch and store a byte
  at the address on top of stack; `c@` zero-extends it to pointer
  width.
- `plic bar s<` leaves 1 on the stack if plic is signed less than bar, 0
  otherwise.
- `plic bar u<` is analogous, but uses an unsigned comparison.
- `pop` discards an integer value from the operand stack; `pop.`
  discards a floating-point value.

### Ten kinds of primitive data ###

- `my plic` defines `plic` as a data label which may be used as a
  statically allocated variable address.
- 372 pushes the number 372 (hexadecimal 174) on the operand stack;
  similarly for other numbers.
- An ASCII doublequote `"` begins a bytestring which continues until
  the next unescaped `"`; this string, once unescaped, is appended to
  data space.  Within the string, you can escape `"` or `\` by
  preceding them with `\`.  As in PostScript, but unlike in Forth,
  there is no space following the beginning delimiter; the string "
  hi" contains three bytes, not two.
- The operations `c,` and `,` take an item from
  the compile-time stack and append it to the data space, occupying
  one byte or one pointer-sized unit, respectively.
  So, for example, `my lf [ 10 c, ]`
  defines the data label `lf` and initializes it with the single byte
  10 ("\n").  `my lf [ 10 ] c,` does the same thing.  `my lf 10 c,`
  does not do what you want because the `10` compiles code at the
  current code position to push the number 10 on the run-time operand
  stack.
- The operation `fill` appends a specified number of zero bytes to
  the data space.
- The operations `calign` and `dalign` append zero bytes to,
  respectively, the code and data spaces, until the address is a
  multiple of the number at the top of the compile-time stack.  That
  number is discarded.

### Two ways to look within oneself ###

- `cell` pushes the number of bytes in a pointer-sized unit onto the
  operand stack.
- `endianness` pushes a value on the operand stack that indicates the
  endianness of the target machine: 0 if little-endian, 1 if
  big-endian, maybe something else someday for NUXI and other strange
  cases.

These are especially useful at compile time.

### Ten means of abstraction ###

- `to plic` defines `plic` as a code label which may be invoked as a
  function or merely jumped to.
- `use plic` defines `plic` as a label which will be provided either
  later in the program or at link time, which is to say, in some
  target-dependent fashion.  You might `use printf`, for example, and
  although a C target will provide `printf` directly, you may need to
  supply `printf` in a different way on, say, an HTML5 target.  To
  write mutually recursive functions without physically nesting one
  inside the other, you need to predeclare one of them with `use plic`.
- `28 enter`, usually used at the beginning of a function, creates a
  new 28-byte stack frame, stores the current frame pointer into its
  first bytes, and sets the frame pointer to point to it.
- `28 ret` pops a 28-byte stack frame from the stack, restoring the
  frame pointer to the value saved in it, and then returns control to
  the return address popped off the call stack.
- `fp` pushes the current frame pointer value on the operand stack.
  `fp+` adds it to the top item on the operand stack.  `fp+@` does
  that and then fetches the pointer-size value at the resulting
  address.  `fp+!` is the equivalent for storing.  `fp+c@` and `fp+c!`
  do the same thing but fetch and store bytes; `fp+c@` zero-extends
  the byte.

### Seven means of combination ###

The basic control-flow operations are:

- `plic go` transfers control to the label `plic`.
- `plic ? bar : baz ;` pops `plic` from the stack, and if it’s nonzero,
  runs `bar`, or if it’s zero, runs `baz`.  `bar` and `baz` can be
  arbitrarily large sequences of code, and `: baz` can be omitted
  entirely.
- `{ plic -> bar }` is a loop that evaluates `plic`, exits the loop if
  it’s zero, evaluates `bar`, and then repeats, potentially
  indefinitely.

The stacks used by `? : ; { -> }` to save and backpatch addresses are
currently exposed to compile-time Uncorp computations.

A few special operations provide for subroutine calls, returns, thread
context switches, stack unwinding, and coroutine resumption:

- `plic 4 1 call` calls the label `plic` as a function, taking 4
  arguments from the current operand stack and returning 1 return
  value.  It pushes the address of the code after the call onto the
  call stack.  Upon arrival at `plic` the operand stack will have only
  the 4 arguments removed from the caller’s operand stack.
- `plic bar yield` transfers control to `plic` and sets the frame
  pointer to `bar`, leaving the address of the code after the yield
  and the old frame pointer on the operand stack.
- `sp` pushes the current call stack pointer on the operand stack;
  `sp!` sets the current call stack pointer from the operand stack.

The particular way `call` is implemented depends on the target, but on
C targets, it’s intended to be compatible with as wide a range of
function call signatures as possible.

XXX what about fastcall, stdcall, etc.?  I may need to rethink this…

Uncorp implementation
---------------------

Uncorp is mostly not yet implemented.  It will have two primary
implementations: a 32-bit interpreter written in node.js, and a compiler
written in Uncorp.  The compiler, when I write it, will initially
target amd64 and AVR, initially without even a register allocator.
Register allocation, constant folding, and the like should be feasible
in a subsequent iteration, or I’ll have to redesign the language.  In
fact, I’ll probably have to redesign the language anyway.

(In the Node interpreter, the terms “compile-time”, “run-time”,
“compilation mode”, and “interpretation mode” used earlier are pretty
confusing.  The best way to understand it is that the interpreter
*pretends* to compile Uncorp code to machine code and then run it, but
actually it just interprets it in two different ways.  The purpose of
the Node interpreter is to bootstrap Uncorp onto a real CPU.)

AVR is much less important than many other platforms, but it differs
more from amd64 than most other platforms, so it should provide better
feedback on the Uncorp design than more common platforms would.

(Also, although this will probably never matter, it should remain
feasible to get AVR chips without backdoors for a number of years,
while all amd64 chips produced for many years have had hardware
backdoors.)

The compiler does not yet exist; the interpreter has 18 primitives
implemented, so about ⅓, and is successfully running recursive
Fibonacci, but is pretty slow.

What’s wrong with C?
--------------------

Undefined behavior is one problem, of course, as is the related lack
of memory-safety, which Uncorp shares.  But C compilation environments
are also very diverse, and this introduces major portability problems.

Let me give an example.  A friend of mine wanted to compile
[Yeso][../yeso/README.md], which at this point is entirely written in
C.  First, he tried compiling it on his Mac; after some struggles, he
found that he could get Xlib.h with
`CPATH=/Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/System/Library/Frameworks/Tk.framework/Versions/8.5/Headers
make` but not extensions/XShm.h.  He gave up on compiling it on MacOS
and tried later to compile it on Linux.  So he figured out to install
the libx11-dev and libxext-dev packages, but then discovered that
yeso/png.c invoked `memcpy`, but did not `#include <string.h>`.  On my
laptop, that happened not to be a problem because some other header
happened to `#include <string.h>`, probably version 1.2.54 of
`<png.h>`, but his more recent version of `<png.h>` (1.6.35-1) didn’t.
Also, the new version doesn’t define `png_infopp_NULL`.  Yeso defines
a couple of preprocessor macros to enable certain functions,
`_BSD_SOURCE` and `_SVID_SOURCE`; some C libraries that provide
e.g. `random()` only declare it conditionally, to avoid name clashes,
so you need to define these macros to use those functions.  (MacOS
libc and dietlibc are two examples.)  But apparently these are
“deprecated” in more recent versions of glibc, generating a warning,
and I had left `-Werror` turned on in the Makefile, so that warning
was an error.  And then some kind of paranoid error-checking decided
that this was an error in tetris.c:

    char lines_buf[5];
    sprintf(lines_buf, "%d", lines_deleted);

Because, you see, you might have cleared more than 9999 lines in
Tetris, and then you would overflow the buffer.

I turned off `-Werror` in the Makefile (it was probably a bad idea for
distribution) but the overall landscape here is not pretty:

- Users trying to compile your programs on some other operating system
  are at the mercy of the operating system vendor for even basic tools
  like GCC, let alone libraries like Xlib and libpng.
- The calling interface to libraries — even popular, stable libraries
  like libpng, which is 24 years old at this point — can change
  between minor versions.
- Or, if you choose to look at it this way, operating system vendors
  can choose to ship ancient versions of libraries, especially
  popular, stable ones like libpng.  In this case, my LinuxMint from
  2015 had version 1.2.26 from 2008.
- Different standard C libraries can be incompatible, even
  aggressively so as in the case of glibc “deprecating” feature test
  macros that dietlibc and MacOS libc require.
- Standard C libraries and C compilers frequently decide to
  intentionally introduce incompatibilities for two reasons: first, to
  improve performance by taking advantage of undefined behavior even
  if it introduces security holes, and second, to remove support for
  previously-legal behavior because it might have introduced security
  holes, as in the format-string example above.
- Generally speaking you cannot rely on testing to find bugs, such as
  my failure to `#include <string.h>`, because many features have
  unpredictable interactions.

The Karger–Thompson attack
--------------------------

Ken Thompson famously gave his 1984 Turing Award lecture on a backdoor
he had planted in Unix some years before: by modifying the compiler to
insert backdoor code into itself and into the login program, he could
remove the backdoor from the compiler source code but still retain it
in the executable, including into the future.  This attack had been
published in 1974 by Paul Karger and Roger Schell, as a theoretical
vulnerability, but Thompson reduced it to practice and made it famous.

Uncorp is built using a JS interpreter such as node.js, which executes
the Uncorp bootstrap interpreter written in JS.  This interpreter can
execute any of the Uncorp compilers that are written in Uncorp,
producing, for example, ELF .o files for amd64 Linux or for Arduino,
or executables for the Intranin kernel of BubbleOS.  This permits
three forms of defense against the Karger–Thompson attack:

1. By bootstrapping from the JS interpreter, the user is safe from
   Karger–Thompson backdoors that could hypothetically be hidden in an
   Uncorp executable.  However, they are potentially vulnerable to
   analogous hypothetical backdoors included in the JS interpreter,
   which could insert backdoors into the Uncorp executable.

2. By compiling the same Uncorp source code using an Uncorp compiler
   running under the JS interpreter and using an Uncorp executable
   built without that particular JS interpreter in its provenance, the
   user can produce two executables that ought to be identical.  If
   not, the user has detected a bug that causes the emission of
   nondeterministic and probably incorrect code — either intentionally
   introduced or not.  This is Wheeler’s “Diverse Double-Compiling”
   defense measure.

3. Because there are several independently-written and -maintained JS
   interpreters — the V8 used in Node, Mozilla SpiderMonkey, Rhino,
   JavaScriptCore, nginx’s njs, and others — the user can run the
   Uncorp interpreter under two different JS interpreters, providing
   the same kind of defense.

Since, for the foreseeable future, BubbleOS is a little-known hobby
project, we can be quite sure that nobody on the V8 or Node teams is
injecting backdoors into the JS interpreter code targeting it, so
Uncorp executables produced in the next few years should be viable for
defense #2.

This, in turn, secures the entire BubbleOS system against the
Karger–Thompson attack at any software level.

A note about word sizes
-----------------------

In the description of the ALU operations, I used the elocution
“pointer-sized” a lot.  The operand stack consists of pointer-sized
values; “pointer-sized” can be 16 bits, 32 bits, or 64 bits.  Most
CPUs of the 1990–2020 vintage can do arithmetic and logic on
pointer-sized values just as fast as they can do it on smaller values,
although the underlying illogic of that is starting to creep through
in the form of power consumption, SIMD operations, and GPGPU.  8-bit
CPUs are an exception, since they usually have 16-bit pointers despite
having only 8-bit ALUs, but I expect the slowdown from a naïve
implementation there to be pretty modest.  A sufficiently smart
compiler could notice that a particular division-free,
right-shift-free tree of operations produces a result that is
truncated to 8 bits, so it can compute the whole tree in 8 bits.

This means that your math is only guaranteed to be good to 16 bits.
For some applications, this is fine — either you don’t care about
portability to machines with less than 32 bits, or your arithmetic
relates to the number of things in memory, or you’re counting things
that are inherently limited in number.  Others need to not overflow so
easily.  If you want 32-bit or 64-bit math, you need to call 32-bit or
64-bit math functions.  Using `cell`, you can conditionally define
them so that on 32-bit and 64-bit machines, they just invoke the basic
ALU operations (I’m still not sure if Uncorp compile-time computation
should be sufficiently powerful enough to allow you to inline this
manually, but certainly a sufficiently smart compiler would inline
such functions) and only on 16-bit-pointer machines do they run
through a whole multi-precision dance.

It also means that if you want to access in-memory data with a known
layout (e.g., a little-endian 32-bit value followed by two
little-endian 16-bit values), you need to use `c@` and `c!` to access
the individual bytes and use ALU operations to translate to the
internal representation.  Again, a sufficiently smart compiler could
recognize these and convert them to individual instructions or mere
addressing modes.

This is pretty similar to ANS FORTH’s model of memory and arithmetic.

Background
----------

There are currently five important CPU instruction sets: amd64, 32-bit
little-endian ARM, AArch64, JS, and the JVM.  There are five important
operating-system interfaces: Linux, Win32/64, HTML5, the JVM, and
Android Dalvik.  There are also a number of secondary or up-and-coming
architectures and system interfaces, including i386, BIOS, UEFI, RISC-V,
MacOS, Android NDK, IBM 360 (they keep trying to rename it and I’ve
lost track of whether it’s still “zSeries” or not), MIPS, WebAssembly,
Xen, big-endian ARM, SPARC, IBM POWER, AVR, Intel GEN, AMD GPU,
Vulkan, NVIDIA GPU, and so on.

In the early decades of high-level programming languages, it was noted
that there was a lot of redundant effort involved in writing compilers
for different programming languages and instruction sets.  An estimate
in the late 1960s was that there were some 700 programming languages,
and there were also hundreds of incompatible instruction sets.  So
most possible language–machine combinations did not have a compiler, a
situation which contributed and contributes greatly to the reduction
in the number of viable machine architectures, and which has resulted
in the loss of an incalculable but enormous amount of software.

A proposed solution in 1958 was “UNCOL”, a “universal compiler-oriented
language”, which would be implemented for many different
architectures, and which new compilers would produce as their output.
The closest equivalent we got to UNCOL for many years was C, but now
LLVM is a better option for many purposes; and C is simultaneously an
increasingly poor fit for new architectures, and an increasingly
problematic language due to a combination of higher security
requirements, extremely problematic language standards, and LP64 ABIs
that prioritized compatibility with existing software over efficiency
for new software.

Uncorp is designed as a better UNCOL than C or LLVM, at least for some
utility function.

#!/usr/bin/env node
// -*- js3 -*-
"use strict";
// Uncorp interpreter.

const fs = require('fs')
    , libs = {log: (d) => process.stdout.write(''+d+'\n')}
    , trace = false
    , builtins = {}

function immediate(func) {
  func.immediate = true
  return func
}

builtins.use = immediate(function use(stack, context) {
  const symbol = context.word()
  context.dict[symbol] = (stack) => stack.push(symbol)
})

builtins.to = immediate(function def(stack, context) {
  const symbol = context.word()
      , here = context.code.length
  context.dict[symbol] = (stack) => stack.push(here)
})

builtins.my = immediate(function my(stack, context) {
  const symbol = context.word()
      , dp = context.data.length
  context.dict[symbol] = (stack) => stack.push(dp)
})

builtins['@'] = function fetch(stack, context) {
  stack.push(context.data[stack.pop()])
}

builtins['!'] = function store(stack, context) {
  const addr = stack.pop()
  context.data[addr] = stack.pop()
}

builtins[','] = immediate(function compile_cell(stack, context) {
  context.data.push(stack.pop())
})

builtins.pop = stack => stack.pop()

builtins.swap = stack => {
  const tos = stack.pop()
  const nos = stack.pop()
  stack.push(tos, nos)
}

builtins.dup = stack => {
  const tos = stack.pop()
  stack.push(tos, tos)
}

builtins.over = stack => stack.push(stack[stack.length-2])

builtins.go = (stack, context) => context.pc = stack.pop()


builtins['+'] = function __add__(stack) {
  const tos = stack.pop()
  stack.push(stack.pop() + tos)
}

builtins['s<'] = function slt(stack) {
  const tos = stack.pop()
  stack.push(stack.pop() < tos ? 1 : 0)
}

builtins['-'] = function __sub__(stack) {
  const tos = stack.pop()
  stack.push(stack.pop() - tos)
}


builtins['['] = immediate((stack, context) => context.compiling = false)
builtins[']'] = immediate((stack, context) => context.compiling = true)

builtins.call = function __call__(stack, context) {
  const addr = stack.pop()
    , n_returns = stack.pop()
    , n_args = stack.pop()

  if (typeof(addr) === 'number') {
    // XXX maybe we should make a stack stack but for now we’re just
    // ignoring the n_returns and n_args
    context.rstack.push(context.pc)
    context.pc = addr
    return
  }

  const args = stack.splice(stack.length - n_args, n_args)
      , results = libs[addr].apply(null, args)
  stack.push(results)  // for now we only support 1-arg lib funcs
  for (let i = 1; i < n_returns; i++) stack.push(undefined)
}

builtins.ret = function ret(stack, context) {
  context.pc = context.rstack.pop()
}

// "If", `?`, compiles to a conditional jump to some location to be
// determined later.  It pushes the code to resolve that jump onto the
// compile-time stack.
// Note that none of the if-else stuff works at compile-time yet.
builtins['?'] = immediate(function __if__(stack, context) {
  // Time of compiling `?`
  const here = context.code.length
  context.code.push('unresolved if')
  stack.push((stack, context) => {
    // Time of compiling `:` or `;`
    const there = context.code.length
    context.code[here] = (stack, context) => {
      // Time of executing `?`
      if (stack.pop() !== 0) {
        if (trace) console.log("conditional true")
      } else {
        if (trace) console.log("conditional false")
        context.pc = there
      }
    }
  })
})

// Endif (`;`) simply invokes the continuation left by if `?` or else
// `:` on the compile-time stack.
builtins[';'] = immediate(function endif(stack, context) {
  stack.pop()(stack, context)
})

// "Else", `:`, does something similar to both: it invokes the
// continuation left by `?`, but also adds an *unconditional* jump to
// some location to be determined later.
builtins[':'] = immediate(function __else__(stack, context) {
  // Time of compiling `:`
  const here = context.code.length
  context.code.push('unresolved else')
  stack.pop()(stack, context)    // resolve the if jump
  stack.push((stack, context) => {
    // Time of compiling `;`
    const there = context.code.length
    context.code[here] = (stack, context) => context.pc = there
  })
})

function terp(s) {
  const dict = {}
      , words = s.split(/\s+/)
      , context = {dict: dict,
                   data: [],
                   code: [],
                   rstack: [],
                   pc: 0,
                   word: () => words[++context.pc],
                  }

  for (let word in builtins) dict[word] = builtins[word]

  compile(words, [], context)
  run('main', [], context)
}

function run(entrypoint, stack, context) {
  context.pc = 0
  context.compiling = false
  context.rstack = ['sentinel']
  context.dict[entrypoint](stack, context)
  context.pc = stack.pop()
  if (trace) console.log("on entering", entrypoint, "rstack is", context.rstack)

  while (context.rstack.length) {
    const def = context.code[context.pc++]
    if (trace) console.log(context.rstack, context.pc, "inner interpreting", def, stack)
    def(stack, context)
  }
}

function compile(words, stack, context) {
  context.compiling = true

  for (context.pc = 0; context.pc < words.length; context.pc++) {
    let word = words[context.pc]
      , def = context.dict[word]
    if (trace) console.log(context.code.length, context.compiling ? "compiling" : "interpreting", word, stack)

    if (def) {
      // if (trace) console.log("found", word, "as", def)
      if (def.immediate || !context.compiling) def(stack, context)
      else context.code.push(def)
      continue
    }

    const number = +word
    if (!isNaN(number)) {
      if (context.compiling) context.code.push(function lit(stack) { stack.push(number) })
      else stack.push(number)
      continue
    }

    throw Error(word)
  }
}

terp(fs.readFileSync(process.argv[2], {encoding: 'utf8'}))

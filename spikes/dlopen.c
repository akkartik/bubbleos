// Test how fast calling a shared library is.
// On my machine it’s apparently 12.5 ns in this case.

#include <stdio.h>
#include <dlfcn.h>

int
main()
{
  char *libm_name = "libm.so.6";
  void *libm = dlopen(libm_name, RTLD_NOW);
  if (!libm) {
    perror(libm_name);
    return -1;
  }
  double (*fabs)(double) = dlsym(libm, "fabs");
  if (!fabs) {
    perror("fabs");
    return -1;
  }
  for (int i = 0; i < 1000*1000*1000; i++) fabs(3.14159);

  dlclose(libm);
}

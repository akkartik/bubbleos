/* Version of fbzoneplate.c with almost no error checking, reducing it
 * to 22 lines of code.
 *
 * This is assuming:
 * - that the ioctls succeeded;
 * - that the framebuffer is page-aligned;
 * - that the framebuffer is of type `FB_TYPE_PACKED_PIXELS` with visual
 *   `FB_VISUAL_TRUECOLOR`;
 * - that the mmap succeeds.
 *
 * It *does* check that the open succeeded and that the framebuffer
 * bit depth is correct.
 *
 * It will probably report a violation of any of these as a segfault,
 * although that is of course not guaranteed.
 */
#include <stdint.h>
#include <fcntl.h>
#include <linux/fb.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <stdio.h>

struct fb_fix_screeninfo fix;
struct fb_var_screeninfo var;

int main()
{
  int fb = open("/dev/fb0", O_RDWR);
  if (fb < 0) return (perror("/dev/fb0"), -1);
  ioctl(fb, FBIOGET_FSCREENINFO, &fix);
  ioctl(fb, FBIOGET_VSCREENINFO, &var);
  uint32_t line_len = var.xres_virtual, b = var.bits_per_pixel;
  uint16_t *p;
  if (b != 16) return fprintf(stderr, "%u != 16\n", b);
  p = (uint16_t*)mmap(0, fix.smem_len, PROT_READ|PROT_WRITE, MAP_SHARED, fb, 0);

  for (uint32_t y = 0; y != var.yres_virtual; y++) {
    for (uint32_t x = 0; x != line_len; x++) p[x + line_len * y] = x*x + y*y;
  }

  return 0;
}

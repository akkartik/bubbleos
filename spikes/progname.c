// A spike to see if we can usually find resources via a relative path
// from the invoking executable.  Linux offers two interfaces that may
// work: program_invocation_name (an alias for argv[0]) and
// /proc/self/exe.  It seems like probably /proc/self/exe is the
// better option, since argv[0] won’t work when the program was found
// on PATH (aside from the possibility that someone invoked it in a
// deliberately obtuse way).  That won’t work if /proc isn’t mounted,
// but even typical Docker environments provide /proc.  Even dietlibc
// provides `program_invocation_name` for compatibility — but it’s
// still usually useless.
// readlink("/proc/self/exe");
// https://sourceware.org/git/?p=glibc.git;a=blob;f=misc/init-misc.c;h=2a1b82710ec8b42b4dac6edb359d8920f902cd21;hb=HEAD
// https://www.tedunangst.com/flak/post/a-prog-by-any-other-name says this is from BSD
// extern char *__progname;
// glibc
// char *__progname_full;
// Aha, this is the actually documented interface <https://www.gnu.org/software/libc/manual/html_node/Error-Messages.html>
// It’s declared in errno.h if you #define _GNU_SOURCE.
// char *program_invocation_name;

#define _GNU_SOURCE  // for program_invocation_name
#define _BSD_SOURCE  // for readlink
#include <errno.h>   // for program_invocation_name
#include <unistd.h>  // for readlink
#include <stdio.h>

int
main()
{
  char buf[512];
  printf("%s\n", program_invocation_name);
  ssize_t len = readlink("/proc/self/exe", buf, sizeof buf);
  if (len >= sizeof buf) len = (sizeof buf) - 1;
  buf[len] = '\0';
  printf("%s\n", buf);
  return 0;
}

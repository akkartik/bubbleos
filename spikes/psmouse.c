/* Receive and decode basic PS/2 mouse protocol, as received on
 * /dev/input/mice or /dev/input/mouse1 or /dev/psaux.
 */

#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>

int main()
{
  char buf[4];
  int x = 0, y = 0;
  for (;;) {
    int n = read(0, buf, sizeof buf);
    if (!n) return 0;
    printf("%d bytes:", n);
    for (int i = 0; i < n; i++) {
      printf(" %02x", (int)(unsigned char)buf[i]);
    }

    if (n == 3) {
      int left = buf[0] & 0x10, down = buf[0] & 0x20;
      int dx = (int)(unsigned char)buf[1] - (left ? 256 : 0);
      int dy = (int)(unsigned char)buf[2] - (down ? 256 : 0);
      x += dx;
      y += dy;
      printf(" (%3d, %3d) %s%s%s"
             //" Δx=%d(%c) Δy=%d(%c)"
             , x, y
             , buf[0] & 1 ? " left" : ""
             , buf[0] & 4 ? " middle" : ""
             , buf[0] & 2 ? " right" : ""
             // , dx, left ? '-' : '+'
             // , dy, down ? '-' : '+'
      );
    }

    printf("\n");
  }
}

/* How easy is it to read JPEGs with libjpeg?  This easy. */

#include <stdio.h>
#include <jpeglib.h>
/*
Code mostly from
/usr/share/doc/libjpeg-turbo8-dev/libjpeg.txt.gz
*/

int
main(int argc, char **argv)
{
  if (argc < 2) {
    fprintf(stderr, "Usage: %s foo.jpeg [bar.jpeg [baz.jpeg...]]\n",
            argv[0]);
    return 2;
  }

  for (int i = 1; i < argc; i++) {
    struct jpeg_decompress_struct cinfo;
    struct jpeg_error_mgr jerr;
    cinfo.err = jpeg_std_error(&jerr);
    jpeg_create_decompress(&cinfo);

    FILE *infile = fopen(argv[i], "rb");
    if (!infile) {
      fprintf(stderr, "can't open %s: ", argv[i]);
      perror("");
      return 1;
    }

    jpeg_stdio_src(&cinfo, infile);
    jpeg_read_header(&cinfo, TRUE); // XXX what is truth?  (require_image)
    /* XXX somehow ask for grayscale to become color?  I have no
       grayscale JPEGs handy so I can't test */
    jpeg_start_decompress(&cinfo);

    printf("%5d × %5d, %d/%d/%d %s\n",
           cinfo.output_width,
           cinfo.output_height,
           cinfo.num_components,
           cinfo.out_color_components,
           cinfo.output_components,
           argv[i]);

    // Don’t do this if not reading scanlines: jpeg_finish_decompress(&cinfo);
    jpeg_destroy_decompress(&cinfo);
    fclose(infile);
  }

  return 0;
}

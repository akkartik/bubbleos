/* Receive and decode Intellimouse PS/2 mouse protocol, using Linux’s
 * emulation on /dev/input/mice.  See
 * <https://www.win.tue.nl/~aeb/linux/kbd/scancodes-13.html>.
 */
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>

int main()
{
  char *mousedev = "/dev/input/mice";
  int mfd = open(mousedev, O_RDWR);
  if (mfd < 0) {
    perror(mousedev);
    return 1;
  }

  /* This is the magic Intellimouse activation secret knock */
  if (6 != write(mfd, "\xf3\xc8\xf3\x64\xf3\x50", 6)) {
    perror("write");
    return 1;
  }

  char buf[5];
  int x = 0, y = 0, z = 0;
  for (;;) {
    int n = read(mfd, buf, sizeof buf);
    if (!n) return 0;
    printf("%d byte%s:", n, n == 1 ? "" : "s");
    for (int i = 0; i < n; i++) {
      printf(" %02x", (int)(unsigned char)buf[i]);
    }

    /* Intellimouse packets are 4 bytes, the fourth being the mouse wheel */
    if (n == 4) {
      int left = buf[0] & 0x10, down = buf[0] & 0x20;
      int dx = (int)(unsigned char)buf[1] - (left ? 256 : 0);
      int dy = (int)(unsigned char)buf[2] - (down ? 256 : 0);
      x += dx;
      y += dy;
      z += (int)(signed char)buf[3];
      printf(" (%3d, %3d, %3d) %s%s%s"
             //" Δx=%d(%c) Δy=%d(%c)"
             , x, y, z
             , buf[0] & 1 ? " left" : ""
             , buf[0] & 4 ? " middle" : ""
             , buf[0] & 2 ? " right" : ""
             // , dx, left ? '-' : '+'
             // , dy, down ? '-' : '+'
      );
    }

    printf("\n");
  }
}

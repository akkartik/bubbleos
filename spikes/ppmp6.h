/* PPM P6 μlibrary. */

typedef struct {
  double r, g, b;
} ppm_p6_color;

void ppm_p6_output_header(int ww, int hh);
void ppm_p6_encode_color(ppm_p6_color co);
void ppm_p6_output_image(ppm_p6_color *pixels, int ww, int hh);


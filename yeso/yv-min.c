/* Minimal X-Windows, fbcon, and Wercam PNG display program with Yeso */
#include <yeso.h>

int main(int argc, char **argv)
{
  ypic img = yp_read_png(argv[1]);
  if (!img.p) return 1;
  ywin w = yw_open(argv[1], img.size, "");
  yp_copy(yw_frame(w), img);
  yw_flip(w);
  for (;;) yw_wait(w, 0);
}

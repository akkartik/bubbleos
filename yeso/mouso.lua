#!/usr/bin/luajit
-- Test app for mouse event handling in yeso.lua.
local yeso = require 'yeso'

function bounds(a, b)
   if a < b then return a, b else return b, a end
end

yeso.Window("mΩuso", {256, 256}):run(function (w)
      local m, dragStart = {x=128, y=128}
      while true do
         for ev in w:events() do
            if ev.isMouse then
               m = ev.p
               if ev:button(1) and dragStart == nil then
                  dragStart = {x=ev.p.x, y=ev.p.y}
               elseif not ev:button(1) then
                  dragStart = nil
               end
            end
         end

         w:frame(function (f)
               f:fill(0x7f7f7f)

               if dragStart then
                  local minX, maxX = bounds(dragStart.x, m.x)
                  local minY, maxY = bounds(dragStart.y, m.y)
                  f:sub({minX, minY}, {maxX-minX, maxY-minY}):fill(0x336699)
               end
         end)

         w:wait()
      end
end)

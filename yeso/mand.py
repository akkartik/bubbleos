#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Render the Mandelbrot set with escape-time coloring in pure Python with Yeso.

Works in CPython 2, CPython 3, and PyPy (tested in PyPy 5.1.2).  PyPy
runs several times faster.

You could do a bunch of optimizations to speed this up and
incrementalizations to make it display responsively, but you’d
probably be better off using a faster language.

"""
from __future__ import division

import yeso


def mandpix(c, palette, lake):
    z = c                # Starting at 0 would just waste an iteration
    for color in palette:
        if abs(z) > 2:
            return color
        z = z**2 + c

    return lake

def mand(fb, palette, lake=0x00007f):
    scale = 2 / min(fb.size.x, fb.size.y)
    center = (fb.size.x + fb.size.y * 1j) / 2

    for y, p in enumerate(fb):
        for x in range(fb.size.x):
            p[x] = mandpix(scale * (x + 1j*y - center), palette, lake)

if __name__ == '__main__':
    with yeso.Window(u"Mandelbrot", (320, 200)) as w:
        while True:
            with w.frame() as fb:
                mand(fb, [191**n for n in range(64)])
            w.wait()

// Yeso path search: for finding Yeso resources such as fonts.
#define _BSD_SOURCE
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <bsd/string.h>

#include <yeso.h>

static void
try(char *dirname,
    char *filename,
    void (*callback)(void *userdata, char *pathname),
    void *userdata)
{
  char buf[512];
  (void)strlcpy(buf, dirname, sizeof buf);
  (void)strlcat(buf, "/", sizeof buf);
  size_t len = strlcat(buf, filename, sizeof buf);
  if (len >= sizeof buf) return;
  struct stat statbuf;
  if (stat(buf, &statbuf) == 0) callback(userdata, buf);
}

void
ypathsea(char *filename,
         void (*callback)(void *userdata, char *pathname),
         void *userdata)
{
  char *envdir = getenv("YESO_DIR");
  if (envdir) try(envdir, filename, callback, userdata);

  // <https://stackoverflow.com/questions/1023306/finding-current-executables-path-without-proc-self-exe/1024937>
  // suggests some alternatives, but this should at least compile on
  // non-Linux systems.  <https://github.com/moby/moby/issues/18883>
  // says Docker on old versions of Linux (pre-4.4) will get EPERM.
  char buf[512];
  ssize_t len = s32_min(sizeof buf - 1,
                        readlink("/proc/self/exe", buf, sizeof buf));
  if (len < 0) return;  // XXX indicate error somehow?
  buf[len] = '\0';
  char *slash = strrchr(buf, '/');
  if (!slash) return;
  slash[1] = '\0';
  try(buf, filename, callback, userdata);

  slash[1] = '\0';
  if (strlcat(buf, "data/", sizeof buf) < sizeof buf) {
    try(buf, filename, callback, userdata);
  }
}

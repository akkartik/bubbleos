/* Abstraction layer to allow me to test a C-implemented RPN calc
 * under Linux before burning it to an AVR hooked up to a scavenged
 * calculator display and keyboard.
 */

typedef char rpn_key;       /* 0 indicates exit — impossible on AVR */
void rpn_setup();
rpn_key rpn_getch();
void rpn_cleanup();
void rpn_set_screen(const char *numbers_and_decimals, int len);

/* Terminal emulator μlibrary, implementing most of the ADM3A. */

typedef struct _admu {
  unsigned char *screenchars;
  // In terms of characters; the 4th column of the second line is 84
  // if cols == 80.
  int cursor_pos;
  int rows, cols;
  int bell;       // True if the bell has rung; reset to 0 if desired.
  // Internal state variables:
  int pending_cup;
  enum { admu_ready, admu_esc, admu_cup, admu_cup2 } state;
} admu;

/* Call this to initialize an admu struct you have allocated with a
 * cols*rows buffer you have allocated.
 */
void admu_init(admu *t, int cols, int rows, unsigned char *screenchars);
/* Call this to make the terminal respond to a received character. */
void admu_handle_char(admu *t, unsigned char c);

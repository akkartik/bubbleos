/* TV typewriter using admu (adμ?).  Echoes keystrokes back to the
 * screen.
 */
#include <yeso.h>
#include "admu.h"
#include "fixedfont.h"

int
main(int argc, char **argv)
{
  enum { rows = 24, cols = 80 };
  ywin w = yw_open("adμ 3a", yp_p(cols*fixedfont_w, rows*fixedfont_h), "");
  admu t;
  unsigned char buf[cols*rows];
  admu_init(&t, cols, rows, buf);
  fixedfont_init();

  for (;;) {
    ypic fb = yw_frame(w);
    for (int y = 0; y < rows; y++) {
      fixedfont_show(yp_sub(fb, yp_p(0, y*fixedfont_h), fb.size),
                     &buf[y*cols], cols, t.cursor_pos - y*cols);
    }
    yw_flip(w);
    yw_wait(w, 0);
    for (yw_event *ev; (ev = yw_get_event(w));) {
      if (yw_as_die_event(ev)) return 0;
      yw_key_event *kev = yw_as_key_event(ev);
      if (kev && kev->down) {
        for (int i = 0; i < kev->len; i++) {
          admu_handle_char(&t, kev->text[i]);
          if (kev->text[i] == '\r') admu_handle_char(&t, '\n');
        }
        if (!kev->len) {
          switch(kev->keysym) {
          case yk_left: admu_handle_char(&t, '\010'); break; /* ^H */
          case yk_down: admu_handle_char(&t, '\012'); break; /* ^J */
          case yk_up: admu_handle_char(&t, '\013'); break;   /* ^K */
          case yk_right: admu_handle_char(&t, '\014'); break; /* ^L */
          }
        }
      }
    }
  }
}

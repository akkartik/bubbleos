// Sketch of Wercam protocol as described in wercam/README.md.
// Currently only handles one client at a time.

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/un.h>
#include <unistd.h>
#include <yeso.h>
#include <stdarg.h>
#include <signal.h>
#include <stdlib.h>


#define LISTENDIR "/tmp/wercam-unix-0"
#define LISTENSOCKET (LISTENDIR "/socket")

static int
open_socket(const char *listensocket)
{
  if (0 != mkdir(LISTENDIR, 0770) && errno != EEXIST) {
    perror(LISTENDIR);
    return -1;
  }

  int s = socket(PF_UNIX, SOCK_SEQPACKET, 0);
  if (s < 0) {
    perror("socket");
    return -1;
  }

  struct sockaddr_un sad;
  sad.sun_family = AF_UNIX;
  strcpy(sad.sun_path, listensocket);

  if (bind(s, (struct sockaddr *)&sad, sizeof sad)) {
    perror(listensocket);
    close(s);
    return -1;
  }

  if (listen(s, 5)) {
    perror("listen");
    close(s);
    return -1;
  }

  return s;
}

// buffer allows us to safely build up a string with a series of
// printf calls so that we can then spit it out to the socket in a
// single write call (important since the socket is SOCK_SEQPACKET!).
// It uses a preallocated buffer that it does not reallocate; instead
// later printf calls will fail harmlessly, setting err to true.  When
// err is true, the building up has run out of memory.
// XXX C&P to yeso-wercam.c

typedef struct {
  char *s;
  size_t len, allocated;
  int err;
} buffer;

buffer
buf_new(int n)
{
  buffer rv = {0};
  rv.s = malloc(n);
  if (rv.s) rv.allocated = n;
  return rv;
}

void
buf_delete(buffer *b)
{
  free(b->s);
}

void
buf_clear(buffer *b)
{
  b->len = b->err = 0;
}

void
buf_printf(buffer *b, char *fmt, ...)
{
  if (b->err) return;
  int limit = b->allocated - b->len;
  va_list args;
  va_start(args, fmt);
  int nl = vsnprintf(b->s + b->len, limit, fmt, args);
  va_end(args);
  if (nl >= limit) b->err = 1;
  else b->len += nl;
}

static void
urlencode(buffer *b, char *t, int n)
{
  for (int i = 0; i != n; i++) {
    int c = t[i];
    // In the interest of readability, we encode control characters
    // even though the protocol doesn’t require it.
    if (c == 0x2b || c == 0x25 || c <= ' ' || c == 0x7f) {
      buf_printf(b, "%%%02X", c & 255);
    } else {
      buf_printf(b, "%c", c);
    }
  }
}

// XXX C&P from yeso-wercam.c
static int
hexdigit(char c)
{
  if ('0' <= c && c <= '9') return (c - '0');
  if ('A' <= c && c <= 'F') return (c - 'A') + 10;
  if ('a' <= c && c <= 'f') return (c - 'a') + 10;
  return 3759;  // maybe some error checking would be better
}

static int
urldecode(char *buf, int len)
{
  int inp = 0, outp = 0;
  while (inp != len) {
    if (buf[inp] == '+') {
      buf[outp] = ' ';
    } else if (buf[inp] == '%' && inp < len - 2) {
      buf[outp] = hexdigit(buf[inp+1]) << 4 | hexdigit(buf[inp+2]);
      inp += 2;
    } else {
      buf[outp] = buf[inp];
    }

    inp++;
    outp++;
  }

  return outp;
}

static int
report(int fd, buffer *b)
{
  int rv = 0;
  if (!b->err) rv = (b->len == write(fd, b->s, b->len));
  buf_clear(b);
  return rv;
}

// We use a SIGINT handler to be able to cleanly exit — although this
// isn’t working in the main loop!  I’m not sure what’s wrong.  strace
// seems to show a poll() on file descriptor 5 (the X11 socket) that
// gets resumed after the signal handler.

static int exit_requested = 0;

static void
sigint_handler(int _)
{
  exit_requested = 1;
}

int
main(int argc, char **argv)
{
  struct sigaction act = {0};
  act.sa_handler = sigint_handler;
  sigaction(SIGINT, &act, 0);

  buffer b = buf_new(2048);
  if (!b.s) {
    perror("malloc");
    return -1;
  }

  char *socket_pathname = argv[1] ? argv[1] : LISTENSOCKET;

  int s = open_socket(socket_pathname);
  if (s < 0) {
    buf_delete(&b);
    return s;
  }
  printf("Listening for a connection on %s\n", socket_pathname);

  int conn = accept(s, 0, 0);
  if (conn < 0) {
    perror("accept");
    // XXX this cleanup really ought to happen in a supervisor child
    // process, not here, because among other things a failure in
    // XOpenDisplay (e.g. because DISPLAY is not set), from a client
    // kill before the first `yw_as_die_event`, or a segfault will
    // exit the program unceremoniously.
    buf_delete(&b);
    unlink(socket_pathname);
    rmdir(LISTENDIR);
    return -1;
  }
  fcntl(conn, F_SETFL, O_NONBLOCK | fcntl(conn, F_GETFL, 0));

  ywin w = 0;
  for (;;) {
    for (yw_event *ev; w && (ev = yw_get_event(w));) {
      yw_key_event *kev = yw_as_key_event(ev);
      if (kev) {
        buf_printf(&b, "key %s %d", kev->down ? "down" : "up", kev->keysym);
        if (kev->len) {
          buf_printf(&b, " ");
          urlencode(&b, kev->text, kev->len);
        }
        report(conn, &b);
      }

      yw_mouse_event *mev = yw_as_mouse_event(ev);
      if (mev) {
        buf_printf(&b, "mouse %d %d %d", mev->p.x, mev->p.y, mev->buttons);
        report(conn, &b);
      }

      if (yw_as_die_event(ev)) {
        buf_printf(&b, "window closed");
        report(conn, &b);
        exit_requested = 1;
      }
    }

    if (w && exit_requested) {
      yw_close(w);
      buf_delete(&b);
      unlink(socket_pathname);
      rmdir(LISTENDIR);
      return 0;
    }

    fd_set fds;
    int max_fd = conn;
    FD_ZERO(&fds);
    FD_SET(conn, &fds);
    if (w) {
      for (int *fdp = yw_fds(w); *fdp >= 0; fdp++) {
        max_fd = s32_max(*fdp, max_fd);
        FD_SET(*fdp, &fds);
      }
    }
    select(max_fd+1, &fds, 0, 0, 0);


    int fd;                     // sent by client in a draw call
    union {
      char buf[CMSG_SPACE(sizeof fd)];
      struct cmsghdr align;
    } u;
    struct iovec iov = {b.s, b.allocated-1};
    struct msghdr msg = {
      .msg_iov = &iov, .msg_iovlen = 1,
      .msg_control = u.buf, .msg_controllen = sizeof u.buf,
    };

    int read_result = recvmsg(conn, &msg, 0);
    if (read_result < 0 && (errno == EAGAIN || errno == EWOULDBLOCK)) continue;
    if (read_result < 0) {
      perror("recvmsg");
      exit_requested = 1;
      continue;
    }

    if (read_result == 0) {     /* connection closed */
      exit_requested = 1;
      continue;
    }
    b.len = read_result;
    buf_printf(&b, "\n");
    report(1, &b);

    // XXX rewrite this, this is a raw prototype
    char *spc = strchr(b.s, ' ');     // title
    char *spc2 = strchr(spc+1, ' ');  // frameno
    char *spc3 = strchr(spc2+1, ' '); // width
    char *spc4 = strchr(spc3+1, ' '); // height
    int width = atoi(spc3+1), height = atoi(spc4+1);

    if (!w) {
      char *title = spc + 1;
      title[urldecode(title, spc2 - title)] = '\0';
      w = yw_open(title, yp_p(width, height), "");
    }

    // Did we get a file descriptor?
    struct cmsghdr *cmsg = CMSG_FIRSTHDR(&msg);
    if (!cmsg) continue;
    if (cmsg->cmsg_level != SOL_SOCKET) continue;
    if (cmsg->cmsg_type != SCM_RIGHTS) continue;
    if (cmsg->cmsg_len != CMSG_LEN(sizeof fd)) continue;
    memcpy(&fd, CMSG_DATA(cmsg), sizeof fd);

    size_t framesize = width * height * 4;
    if (ftruncate(fd, framesize) < 0) perror("ftruncate");
    void *filedata = mmap(0, framesize, PROT_READ | PROT_WRITE, MAP_SHARED,
                          fd, 0);
    if (!filedata || (intptr_t)filedata == -1) {
      perror("mmap");
      close(fd);
      continue;
    }
    ypic f = yw_frame(w);

    ypic frame = {.p = filedata, .size = yp_p(width, height), .stride=width};
    yp_copy(f, frame);
    munmap(filedata, framesize);
    yw_flip(w);

    if (close(fd)) perror("close of passed file descriptor");

    buf_printf(&b, "window open %d %d %d", atoi(spc2+1), f.size.x, f.size.y);
    report(conn, &b);
  }
}

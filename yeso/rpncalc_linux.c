/* Provide a Linux terminal UI for rpncalc.c.
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "rpncalc.h"

void rpn_set_screen(const char *numbers, int len) {
  char buf[len+1];
  memcpy(buf, numbers, len);
  buf[len] = '\0';
  printf("\r%s        \r", buf);
  fflush(stdout);
}

void rpn_setup() {
  printf("Type q to exit, 3 4+ to add 3 and 4, etc.\n"
         "Since this is a testing harness for a pocket calculator UI,\n"
         "you only see up to 8 digits at once.\n");
  int n = system("stty -echo cbreak");
  n = n;
}

/* This is somewhat lossy but should be adequate to the purpose */
rpn_key rpn_getch() {
  int c = getchar();
  if (c == EOF || c == 'q' || c == 'Q') return 0;
  if (c == 0x7f) return '\b';
  if (c == 0) return -1;
  return c;
}

void rpn_cleanup() {
  int n = system("stty echo -cbreak");
  n = n;
  printf("\n");
}

/* Test the decimal floating-point math in decimal.c.
 *
 * I wanted to make sure I did at least some minimal testing before I
 * slept, but I couldn’t be bothered to make a real pass/fail test
 * suite at the moment.
 */

#include <stdio.h>
#include <stdint.h>
#include "decimal.h"

static void say(decimal n) {
  if (n.is_error) {
    printf("E");
  } else {
    printf("%de-%d", n.mantissa, n.decimals);
  }
}

static void expr(decimal a, char *op, decimal b, decimal result) {
  say(a);
  printf(" %s ", op);
  say(b);
  printf(" = ");
  say(result);
  printf("\n");
}

int main() {
  decimal ten = dec_of_int(10);
  decimal three = dec_of_int(3);
  decimal thirteen = dec_add(ten, three);
  expr(ten, "+", three, thirteen);

  decimal p3 = dec_div(three, ten, 3);
  expr(three, "÷", ten, p3);

  decimal seven = dec_sub(ten, three);
  expr(ten, "-", three, seven);
  expr(ten, "-", p3, dec_sub(ten, p3));
  expr(ten, "·", p3, dec_mul(ten, p3));
  decimal threep = dec_div(ten, three, 5);
  expr(ten, "÷", three, threep);
  expr(seven, "·", threep, dec_mul(seven, threep));

  /* Division by zero errors */
  decimal zero = dec_of_int(0);
  decimal e = dec_div(ten, zero, 3);
  expr(ten, "÷", zero, e);
  expr(e, "+", p3, dec_add(e, p3));
  expr(e, "-", p3, dec_sub(e, p3));
  expr(e, "·", p3, dec_mul(e, p3));
  expr(e, "÷", p3, dec_div(e, p3, 3));

  expr(p3, "+", e, dec_add(p3, e));
  expr(p3, "-", e, dec_sub(p3, e));
  expr(p3, "·", e, dec_mul(p3, e));
  expr(p3, "÷", e, dec_div(p3, e, 3));

  expr(p3, "·", p3, dec_mul(p3, p3));
  expr(three, "÷", p3, dec_div(three, p3, 6));

  return 0;
}

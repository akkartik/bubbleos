// Death clock: estimate the life expectancy of a human body based on
// their birthdate, gender, and country of residence.  The name of
// this program is toki ike, not toki pona.

#include <assert.h>
#include <ctype.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include <yeso.h>

// Yearly death probability by age in the US in 2015, according to the
// actuarial tables at <https://www.ssa.gov/oact/STATS/table4c6.html>

const float m_death[] = {
  0.006383,
  0.000453,
  0.000282,
  0.000230,
  0.000169,
  0.000155,
  0.000145,
  0.000135,
  0.000120,
  0.000105,
  0.000094,
  0.000099,
  0.000134,
  0.000207,
  0.000309,
  0.000419,
  0.000530,
  0.000655,
  0.000791,
  0.000934,
  0.001085,
  0.001228,
  0.001339,
  0.001403,
  0.001433,
  0.001451,
  0.001475,
  0.001502,
  0.001538,
  0.001581,
  0.001626,
  0.001669,
  0.001712,
  0.001755,
  0.001800,
  0.001855,
  0.001920,
  0.001988,
  0.002060,
  0.002141,
  0.002240,
  0.002362,
  0.002509,
  0.002684,
  0.002890,
  0.003121,
  0.003386,
  0.003707,
  0.004091,
  0.004531,
  0.005013,
  0.005524,
  0.006059,
  0.006611,
  0.007187,
  0.007800,
  0.008456,
  0.009144,
  0.009865,
  0.010622,
  0.011458,
  0.012350,
  0.013235,
  0.014097,
  0.014979,
  0.015967,
  0.017109,
  0.018392,
  0.019836,
  0.021465,
  0.023351,
  0.025482,
  0.027794,
  0.030282,
  0.033022,
  0.036201,
  0.039858,
  0.043891,
  0.048311,
  0.053228,
  0.058897,
  0.065365,
  0.072491,
  0.080288,
  0.088916,
  0.098576,
  0.109438,
  0.121619,
  0.135176,
  0.150109,
  0.166397,
  0.183997,
  0.202855,
  0.222911,
  0.244094,
  0.265091,
  0.285508,
  0.304926,
  0.322919,
  0.339065,
  0.356018,
  0.373819,
  0.392510,
  0.412135,
  0.432742,
  0.454379,
  0.477098,
  0.500953,
  0.526000,
  0.552300,
  0.579915,
  0.608911,
  0.639357,
  0.671325,
  0.704891,
  0.740135,
  0.777142,
  0.815999,
  0.856799,
  0.899639,
};

const float f_death[] = {
  0.005374,
  0.000353,
  0.000231,
  0.000165,
  0.000129,
  0.000116,
  0.000107,
  0.000101,
  0.000096,
  0.000092,
  0.000091,
  0.000096,
  0.000111,
  0.000138,
  0.000174,
  0.000214,
  0.000254,
  0.000294,
  0.000330,
  0.000364,
  0.000399,
  0.000436,
  0.000469,
  0.000497,
  0.000522,
  0.000546,
  0.000572,
  0.000604,
  0.000644,
  0.000690,
  0.000740,
  0.000792,
  0.000841,
  0.000886,
  0.000929,
  0.000977,
  0.001034,
  0.001098,
  0.001171,
  0.001253,
  0.001347,
  0.001452,
  0.001571,
  0.001706,
  0.001857,
  0.002022,
  0.002204,
  0.002411,
  0.002648,
  0.002910,
  0.003193,
  0.003491,
  0.003801,
  0.004119,
  0.004449,
  0.004813,
  0.005201,
  0.005583,
  0.005952,
  0.006325,
  0.006749,
  0.007238,
  0.007776,
  0.008368,
  0.009032,
  0.009794,
  0.010673,
  0.011676,
  0.012815,
  0.014105,
  0.015616,
  0.017318,
  0.019118,
  0.020996,
  0.023033,
  0.025413,
  0.028197,
  0.031313,
  0.034782,
  0.038689,
  0.043258,
  0.048490,
  0.054223,
  0.060446,
  0.067338,
  0.075133,
  0.084033,
  0.094177,
  0.105633,
  0.118407,
  0.132476,
  0.147801,
  0.164331,
  0.182012,
  0.200783,
  0.219758,
  0.238630,
  0.257065,
  0.274706,
  0.291189,
  0.308660,
  0.327180,
  0.346810,
  0.367619,
  0.389676,
  0.413057,
  0.437840,
  0.464111,
  0.491957,
  0.521475,
  0.552763,
  0.585929,
  0.621085,
  0.658350,
  0.697851,
  0.739722,
  0.777142,
  0.815999,
  0.856799,
  0.899639,
};

// Crude death rates by country, from
// <https://www.cia.gov/library/publications/the-world-factbook/rankorder/rawdata_2066.txt>.
// The idea is that we presume that the variation of death rate by
// country impacts all age groups equally; for example, if the death
// rate in Argentina is 7.5 (per 1000 population per year), which is
// 8.5% lower than that in the US, 8.2, then we assume that the death
// rate in Argentina in every age group is 8.5% lower.  This is not a
// very good approximation — among other things, countries with older
// populations tend to have higher crude death rates — but maybe it is
// better than assuming that every country has the same death rate.

// This includes some territories that aren’t, strictly speaking,
// countries, such as Guam.

const struct country {
  const char *name;
  const char *code;
} countries[] = {
  {"Lesotho", ""},
  {"Lithuania", ""},
  {"Bulgaria", ""},
  {"Latvia", ""},
  {"Ukraine", ""},
  {"Guinea-Bissau", ""},
  {"Chad", ""},
  {"Serbia", ""},
  {"Russia", "ru"},
  {"Afghanistan", ""},
  {"Central African Republic", ""},
  {"Belarus", ""},
  {"Swaziland", ""},
  {"Somalia", ""},
  {"Gabon", ""},
  {"Hungary", "hu"},
  {"Estonia", "ee"},
  {"Moldova", ""},
  {"Nigeria", ""},
  {"Zambia", ""},
  {"Croatia", ""},
  {"Romania", "ro"},
  {"Niger", ""},
  {"Germany", "de"},
  {"Mozambique", ""},
  {"Slovenia", ""},
  {"Greece", ""},
  {"Burkina Faso", ""},
  {"Portugal", "pt"},
  {"Georgia", ""},
  {"Czechia", ""},
  {"Italy", "it"},
  {"Poland", ""},
  {"Sierra Leone", ""},
  {"Denmark", "dk"},
  {"Zimbabwe", ""},
  {"Isle of Man", ""},
  {"Uganda", ""},
  {"Bosnia and Herzegovina", ""},
  {"Finland", "fi"},
  {"Slovakia", ""},
  {"Saint Pierre and Miquelon", ""},
  {"Mali", ""},
  {"Monaco", ""},
  {"Japan", "jp"},
  {"Belgium", "be"},
  {"Montenegro", ""},
  {"Cameroon", ""},
  {"Botswana", ""},
  {"Austria", "at"},
  {"Congo, Democratic Republic of the", ""},
  {"Congo, Republic of the", ""},
  {"Cote d'Ivoire", ""},
  {"Armenia", ""},
  {"United Kingdom", "uk"},
  {"Uruguay", "uy"},
  {"Sweden", "se"},
  {"South Africa", "za"},
  {"Malta", ""},
  {"France", "fr"},
  {"Korea, North", ""},
  {"Angola", ""},
  {"Macedonia", ""},
  {"Spain", "sp"},
  {"Guernsey", ""},
  {"Guinea", ""},
  {"Netherlands", "nl"},
  {"Faroe Islands", ""},
  {"Burundi", ""},
  {"Trinidad and Tobago", ""},
  {"Greenland", ""},
  {"San Marino", ""},
  {"Canada", "ca"},
  {"Cuba", ""},
  {"Bermuda", ""},
  {"Puerto Rico", ""},
  {"Barbados", ""},
  {"Gibraltar", ""},
  {"Tuvalu", ""},
  {"Cook Islands", ""},
  {"Curacao", ""},
  {"Aruba", ""},
  {"Switzerland", "ch"},
  {"Grenada", ""},
  {"United States", "us"},
  {"Palau", ""},
  {"Norway", "no"},
  {"Kazakhstan", "kz"},
  {"Western Sahara", ""},
  {"Senegal", ""},
  {"Thailand", "th"},
  {"Benin", ""},
  {"Dominica", ""},
  {"Malawi", ""},
  {"Mauritania", ""},
  {"Namibia", ""},
  {"Equatorial Guinea", ""},
  {"Jersey", ""},
  {"Virgin Islands", ""},
  {"Saint Helena, Ascension, and Tristan da Cunha", ""},
  {"China", "cn"},
  {"Ethiopia", ""},
  {"Saint Lucia", ""},
  {"South Sudan", ""},
  {"Liberia", ""},
  {"Haiti", ""},
  {"Tanzania", ""},
  {"Djibouti", ""},
  {"Cambodia", ""},
  {"Argentina", "ar"},
  {"New Zealand", ""},
  {"Guyana", ""},
  {"Taiwan", "tw"},
  {"Burma", ""},
  {"Laos", ""},
  {"Liechtenstein", ""},
  {"Hong Kong", "hk"},
  {"India", "in"},
  {"Australia", "au"},
  {"Andorra", ""},
  {"Saint Vincent and the Grenadines", ""},
  {"Sudan", ""},
  {"Luxembourg", ""},
  {"Comoros", ""},
  {"Bahamas, The", ""},
  {"Eritrea", ""},
  {"Mauritius", ""},
  {"Azerbaijan", ""},
  {"Saint Kitts and Nevis", ""},
  {"Kiribati", ""},
  {"Ghana", ""},
  {"Seychelles", ""},
  {"Gambia, The", ""},
  {"Togo", ""},
  {"Jamaica", ""},
  {"Albania", ""},
  {"Cyprus", ""},
  {"Sao Tome and Principe", ""},
  {"Brazil", "br"},
  {"Kenya", ""},
  {"Papua New Guinea", ""},
  {"Ireland", ""},
  {"Kyrgyzstan", ""},
  {"Bhutan", ""},
  {"Indonesia", ""},
  {"Madagascar", ""},
  {"Bolivia", "bo"},
  {"Iceland", "is"},
  {"Rwanda", ""},
  {"Pakistan", "pk"},
  {"Tunisia", ""},
  {"Mongolia", ""},
  {"Sri Lanka", ""},
  {"Montserrat", ""},
  {"Chile", "cl"},
  {"Philippines", ""},
  {"Peru", "pe"},
  {"Suriname", ""},
  {"Fiji", ""},
  {"Turkmenistan", ""},
  {"Tajikistan", "tj"},
  {"Guam", ""},
  {"Cabo Verde", ""},
  {"Belize", ""},
  {"Turkey", ""},
  {"Yemen", ""},
  {"Korea, South", ""},
  {"American Samoa", ""},
  {"Nauru", ""},
  {"Vietnam", ""},
  {"Timor-Leste", ""},
  {"Cayman Islands", ""},
  {"El Salvador", ""},
  {"New Caledonia", ""},
  {"Antigua and Barbuda", ""},
  {"Nepal", ""},
  {"Colombia", ""},
  {"Bangladesh", ""},
  {"Wallis and Futuna", ""},
  {"Samoa", ""},
  {"Uzbekistan", ""},
  {"Mexico", "mx"},
  {"Iran", "ir"},
  {"Honduras", ""},
  {"Venezuela", "ve"},
  {"French Polynesia", ""},
  {"Israel", "il"},
  {"Sint Maarten", ""},
  {"Nicaragua", ""},
  {"British Virgin Islands", ""},
  {"Ecuador", "ec"},
  {"Malaysia", ""},
  {"Lebanon", ""},
  {"Falkland Islands (Islas Malvinas)", ""},
  {"Morocco", ""},
  {"Tonga", "to"},
  {"Panama", ""},
  {"Paraguay", ""},
  {"Northern Mariana Islands", ""},
  {"Guatemala", ""},
  {"Costa Rica", "cr"},
  {"Dominican Republic", ""},
  {"Egypt", ""},
  {"Anguilla", ""},
  {"Macau", ""},
  {"Algeria", ""},
  {"Micronesia, Federated States of", ""},
  {"Marshall Islands", ""},
  {"Maldives", ""},
  {"Vanuatu", ""},
  {"Syria", ""},
  {"Iraq", "iq"},
  {"Solomon Islands", ""},
  {"Brunei", ""},
  {"Libya", ""},
  {"West Bank", ""},
  {"Singapore", "sg"},
  {"Jordan", ""},
  {"Saudi Arabia", ""},
  {"Oman", ""},
  {"Turks and Caicos Islands", "cc"},
  {"Gaza Strip", ""},
  {"Bahrain", ""},
  {"Kuwait", ""},
  {"United Arab Emirates", ""},
  {"Qatar", ""},
};

const float country_deaths[] = {
  15.00,
  14.60,
  14.50,
  14.50,
  14.40,
  13.90,
  13.80,
  13.60,
  13.50,
  13.40,
  13.20,
  13.20,
  13.20,
  13.10,
  13.00,
  12.80,
  12.60,
  12.60,
  12.40,
  12.20,
  12.20,
  12.00,
  11.80,
  11.70,
  11.60,
  11.60,
  11.30,
  11.20,
  11.10,
  10.90,
  10.50,
  10.40,
  10.40,
  10.40,
  10.30,
  10.20,
  10.20,
  10.20,
  10.00,
  10.00,
  9.90,
  9.90,
  9.80,
  9.80,
  9.80,
  9.70,
  9.70,
  9.60,
  9.60,
  9.60,
  9.60,
  9.50,
  9.40,
  9.40,
  9.40,
  9.40,
  9.40,
  9.40,
  9.40,
  9.30,
  9.30,
  9.20,
  9.20,
  9.10,
  9.00,
  9.00,
  8.90,
  8.80,
  8.80,
  8.80,
  8.70,
  8.70,
  8.70,
  8.70,
  8.60,
  8.60,
  8.60,
  8.50,
  8.50,
  8.40,
  8.40,
  8.40,
  8.30,
  8.20,
  8.20,
  8.10,
  8.10,
  8.10,
  8.10,
  8.10,
  8.00,
  7.90,
  7.90,
  7.90,
  7.90,
  7.90,
  7.80,
  7.80,
  7.80,
  7.80,
  7.80,
  7.70,
  7.70,
  7.70,
  7.60,
  7.60,
  7.60,
  7.50,
  7.50,
  7.50,
  7.50,
  7.40,
  7.40,
  7.40,
  7.40,
  7.40,
  7.40,
  7.30,
  7.30,
  7.30,
  7.30,
  7.30,
  7.30,
  7.20,
  7.20,
  7.20,
  7.10,
  7.10,
  7.10,
  7.00,
  7.00,
  7.00,
  7.00,
  6.90,
  6.80,
  6.80,
  6.80,
  6.80,
  6.70,
  6.70,
  6.60,
  6.60,
  6.50,
  6.50,
  6.50,
  6.50,
  6.40,
  6.40,
  6.40,
  6.30,
  6.30,
  6.30,
  6.20,
  6.20,
  6.20,
  6.10,
  6.10,
  6.10,
  6.10,
  6.10,
  6.00,
  6.00,
  6.00,
  6.00,
  6.00,
  6.00,
  6.00,
  5.90,
  5.90,
  5.90,
  5.90,
  5.80,
  5.80,
  5.70,
  5.70,
  5.60,
  5.50,
  5.40,
  5.40,
  5.30,
  5.30,
  5.30,
  5.30,
  5.30,
  5.30,
  5.20,
  5.20,
  5.20,
  5.10,
  5.10,
  5.10,
  5.10,
  5.00,
  4.90,
  4.90,
  4.90,
  4.90,
  4.80,
  4.80,
  4.70,
  4.70,
  4.70,
  4.60,
  4.60,
  4.50,
  4.30,
  4.20,
  4.20,
  4.00,
  4.00,
  4.00,
  3.80,
  3.80,
  3.60,
  3.60,
  3.50,
  3.50,
  3.40,
  3.40,
  3.30,
  3.20,
  3.10,
  2.80,
  2.20,
  1.90,
  1.50,
};

#define LEN(array) (sizeof(array)/sizeof(*array))

float
us_year_death_risk(int year, char gender)
{
  if (year >= LEN(f_death)) year = LEN(f_death) - 1;
  if (gender == 'm') return m_death[year];
  if (gender == 'f') return f_death[year];
  return (m_death[year] + f_death[year])/2;
}

char *
ordinal_suffix(int n)
{
  switch(n % 10) {
  case 1: return "st";
  case 2: return "nd";
  case 3: return "rd";
  default: return "th";
  }
}

typedef struct {
  int days, hours, minutes;
} toki_result;

typedef struct {
  time_t birth;
  char gender;
  char *country_code;
} toki_model_inputs;

toki_result calculate_remaining(toki_model_inputs model, time_t now, int verbose)
{
  int ci;
  for (ci = 0; ci < LEN(countries); ci++) {
    if (!strcmp(model.country_code, countries[ci].code)) break;
  }

  double base_country_death = 8.2, country_multiplier = country_deaths[ci] / base_country_death;

  long long age = now - model.birth;
  if (verbose) {
    printf("This %c human body in %s is %lld seconds old.\n",
           model.gender, countries[ci].name, age);
  }
  #define days_in_year 365.24219
  #define seconds_in_year (days_in_year * 86400)
  double age_years = age/seconds_in_year;
  if (verbose) printf("This is %.9f tropical years.\n", age_years);

  int age_floor = floor(age_years);
  double age_frac = age_years - age_floor,
    us_death_risk_this_year = us_year_death_risk(age_floor, model.gender),
    death_risk_this_whole_year = country_multiplier * us_death_risk_this_year,
    death_risk_before_birthday = (1 - age_frac) * death_risk_this_whole_year;

  if (verbose) {
    printf("They have a %.9f%% chance of dying in the %.9f years before their next birthday.\n",
           100 * death_risk_before_birthday, 1 - age_frac);
  }

  double survival_to_birthday_chance = 1 - death_risk_before_birthday,
    survival_chance = survival_to_birthday_chance;

  int future_age;
  for (future_age = age_floor + 1; survival_chance > 0.5; future_age++) {
    double death_chance = country_multiplier
      * us_year_death_risk(future_age, model.gender);
    survival_chance *= 1 - death_chance;
    if (!verbose) continue;
    printf("They have a %.2f%% chance of surviving to their %d%s birthday.\n",
           100 * survival_chance, future_age + 1, ordinal_suffix(future_age + 1));
  }

  // Now let’s interpolate.
  int median_death_year = future_age - 1;
  double median_death_chance = country_multiplier
    * us_year_death_risk(median_death_year, model.gender);
  // So the chance of surviving all year is (1 - median_death_chance).
  // It’s an exponential function.  We want to find the exponent that
  // gets us to 50% — from the less-than-50% chance we’ve calculated
  // at the end of the year.
  double year_fraction = log(survival_chance / .5) / log(1 - median_death_chance);

  if (verbose) printf("%d years, minus %.2f.\n", future_age, year_fraction);

  double death_age = future_age - year_fraction;
  long long death_age_seconds = death_age * seconds_in_year;
  if (verbose) printf("Death 50%% probable by %.9f years = %lld seconds.\n", death_age, death_age_seconds);
  long long remaining_seconds = death_age_seconds - age;
  if (verbose) {
    printf("That is %lld seconds in the future.\n",
           remaining_seconds);
  }
  
  toki_result remaining = {
    .days = remaining_seconds / 86400,
    .hours = remaining_seconds % 86400 / 3600,
    .minutes = remaining_seconds % 86400 % 3600 / 60,
  };
  return remaining;
}

int
main(int argc, char **argv)
{
  assert(LEN(m_death) == 120);
  assert(LEN(f_death) == 120);
  assert(LEN(country_deaths) == LEN(countries));

  int verbose = 0, graphical = 0;
  if (argc == 7 && !strcmp(argv[1], "-v")) {
    verbose = 1;
    argv++;
    argc--;
  } else if (argc == 7 && !strcmp(argv[1], "-g")) {
    graphical = 1;
    argv++;
    argc--;
  }

  if (argc != 6) {
    fprintf(stderr, "usage: %s [-v|-g] 1981 12 31 f us\n", argv[0]);
    fprintf(stderr, "Provide birthdate, gender, and two-letter"
            " ISO code for nation of residence.\n");
    return 1;
  }

  struct tm birth_tm = {
    .tm_year = atoi(argv[1]) - 1900,
    .tm_mon = atoi(argv[2]) - 1,
    .tm_mday = atoi(argv[3]),
  };

  toki_model_inputs model = {
    .birth = mktime(&birth_tm),
    .gender = tolower(argv[4][0]),
    .country_code = argv[5],
  };

  int ci;
  for (ci = 0; ci < LEN(countries); ci++) {
    if (!strcmp(model.country_code, countries[ci].code)) break;
  }

  if (ci == LEN(countries)) {
    fprintf(stderr, "Unknown country code %s; try something like 'us', 'ar', or 'uk'.\n",
            model.country_code);
    return 1;
  }

  if (graphical) {
    char buf[100];
    ywin w = yw_open("toki", yp_p(80, 10), "");
    yp_font *font = yp_font_read("cuerdas-caoticas-1.jpeg"), *scaled = 0;
    int old_height = 0;
    for (;;) {
      toki_result remaining = calculate_remaining(model, time(0), verbose);
      sprintf(buf, "%d %02d:%02d", remaining.days, remaining.hours, remaining.minutes);

      ypic g = yw_frame(w);

      if (!scaled || old_height != g.size.y) {
        yp_font_delete(scaled);
        scaled = yp_font_scale(font, g.size.y);
        if (!scaled) abort();
        old_height = g.size.y;
      }

      yp_fill(g, 0xf0f0f0);
      yp_font_show(scaled, buf, g);
      //yp_font_dump(scaled, g);
      yw_flip(w);

      yw_wait(w, 500*1000);
    }
  } else {
    toki_result remaining = calculate_remaining(model, time(0), verbose);

    printf("In %d days, %d hours, %d minutes there is a 50%% chance they will be dead.\n",
           remaining.days,
           remaining.hours,
           remaining.minutes);
  }

  return 0;
}

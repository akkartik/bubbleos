#!/usr/bin/luajit
-- Test app for key event handling in yeso.lua.
local yeso = require 'yeso'

yeso.Window("key evenτ tester", {384, 384}):run(function (w)
      local done = false
      while not done do
         for ev in w:events() do
            if--  ev:isDie() then
            --    print("die")
            --    done = true
            -- elseif 
            ev.isMouse then
               print(("at (%d, %d) with buttons %d"):format(
                        ev.p.x, ev.p.y, ev.buttons))
            elseif ev.isKey then
               print(("key %s %d %s"):format(ev.down == 0 and "up" or "down",
                                             ev.keysym,
                                             ev:s()))
            end
         end
         w:wait()
      end
end)

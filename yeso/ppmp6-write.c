/* PPM P6 file format; see <http://netpbm.sourceforge.net/doc/ppm.html> */

/* (PPM code modified somewhat from
   <http://canonical.org/~kragen/sw/aspmisc/raytracer.c>) */

#include <string.h>
#include <stdio.h>
#include <yeso.h>
#include <errno.h>

static inline void
ppm_p6_encode_color(FILE *f, ypix color)
{
  putc((color >> 16) & 0xff, f);
  putc((color >> 8) & 0xff, f);
  putc(color & 0xff, f);
}

void
yp_write_ppmp6(ypic pic, char *filename)
{
  char buf[strlen(filename) + 255];

  FILE *f = fopen(filename, "wb");
  if (!f) {
    sprintf(buf, "yp_write_ppmp6: %s: %s", filename, strerror(errno));
    yeso_error_handler(buf);
    return;
  }

  fprintf(f, "P6\n%d %d\n255\n", pic.size.x, pic.size.y);
  for (size_t y = 0; y < pic.size.y; y++) {
    ypix *line = yp_line(pic, y);
    for (size_t x = 0; x < pic.size.x; x++) ppm_p6_encode_color(f, line[x]);
  }

  if (ferror(f)) {
    sprintf(buf, "yp_write_ppmp6 end: %s: %s", filename, strerror(errno));
    fclose(f);
    yeso_error_handler(buf);
    return;
  }
  fclose(f);
}

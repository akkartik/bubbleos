/* PPM P6 file format; see <http://netpbm.sourceforge.net/doc/ppm.html> */
#include <stdio.h>
#include <ctype.h>
#include <errno.h>
#include <string.h>
#include <yeso.h>


ypic
yp_read_ppmp6(char *filename)
{
  ypic result = {0};
  char buf[strlen(filename) + 255];

  FILE *f = fopen(filename, "rb");
  if (!f) {
    fprintf(stderr, "yp_read_ppmp6: %s: %s\n", filename, strerror(errno));
    return result;
  }

  char mag1, mag2;              // Magic number
  int width, height, depth;
  if (5 != fscanf(f, "%c%c %d %d %d", &mag1, &mag2, &width, &height, &depth)
      || mag1 != 'P'
      || mag2 != '6'
      || depth != 255) {
    fprintf(stderr, "yp_read_ppmp6: %s: not an 8-bit PPM P6 file\n", filename);
    fclose(f);
    return result;
  }

  getc(f);  // eat the newline we hope is there

  result = yp_new(yp_p(width, height));
  if (!result.p) return result;
  for (int y = 0; y < height; y++) {
    ypix *line = yp_line(result, y);
    for (int x = 0; x < width; x++) {
      unsigned r = getc(f);
      unsigned g = getc(f);
      unsigned b = getc(f);
      line[x] = r << 16 | g << 8 | b;
    }
  }
  
  if (ferror(f)) {
    sprintf(buf, "yp_read_ppmp6 end: %s: %s", filename, strerror(errno));
    fclose(f);
    yeso_error_handler(buf);
    return result;
  }
  fclose(f);

  return result;
}

/** Really dumb windowing library, providing just a shared-memory
 * framebuffer you can flush onto the screen and some events, with
 * different backends.  See
 * README.md for more details.
 */

#ifndef YESO_H_INCLUDED
#define YESO_H_INCLUDED
#include <stdint.h>

typedef uint8_t u8;
typedef int32_t s32;
typedef uint32_t u32;

#ifndef __GNUC__
#define __attribute__(x)
#endif

// This seems to result in smaller code in objdump -d, although for
// some reason it doesn’t make a difference in size -A.
#define inline inline __attribute__ ((__always_inline__))

static inline s32
s32_max(s32 a, s32 b)
{
  return (a > b) ? a : b;
}

static inline s32
s32_min(s32 a, s32 b)
{
  return (a > b) ? b : a;
}

/** `yp_p2` represents a 2-D set of coordinates, normally right and down
 * from some upper-left reference point.  Since compound literals
 * aren’t part of C99, and even if they were they’re kind of
 * syntactically noisy, there’s also a function `yp_p` to construct
 * them.
 */
typedef struct { s32 x, y; } yp_p2;
static inline yp_p2
yp_p(s32 x, s32 y)
{
  yp_p2 p = { .x = x, .y = y };
  return p;
}

/** yp_p_add adds a displacement to a point. */
static inline yp_p2
yp_p_add(yp_p2 a, yp_p2 b)
{
  yp_p2 p = { .x = a.x + b.x, .y = a.y + b.y };
  return p;
}

/** yp_p_sub subtracts two points or a point and a displacement to get,
 * respectively, a displacement or a point.
 */
static inline yp_p2
yp_p_sub(yp_p2 a, yp_p2 b)
{
  yp_p2 p = { .x = a.x - b.x, .y = a.y - b.y };
  return p;
}

/** yp_p_within tells you whether p is within [0, bounds.x) and [0, bounds.y).
 */
static inline int
yp_p_within(yp_p2 p, yp_p2 bounds)
{
  return (u32)p.x < (u32)bounds.x && (u32)p.y < (u32)bounds.y;
}

/* ypix is a 32-bit little-endian RGB TrueColor pixel: the
 * least-significant byte is blue, the next is green, the next is red,
 * and the most significant byte is ignored.  Maybe you could compile
 * some Yeso apps for a platform with smaller pixels by changing this
 * typedef.
 */
typedef u32 ypix;

/** `ypic` — a TrueColor in-memory image — either a framebuffer, or not.
 * (It may be just a plain memory buffer or part of a larger ypic.)
 * `stride` is the number of `ypix` units from the beginning of one
 * scan line to the beginning of the next, and may be greater than
 * size.x.
 */
typedef struct { ypix *p; yp_p2 size; int stride; } ypic;

/** yp_line computes the start address of a scan line in a ypic.  No bounds check.
 */
static inline ypix *
yp_line(ypic c, int y)
{
  return &c.p[y*c.stride];
}

/** Create an in-memory ypic.
 */
ypic yp_new(yp_p2 size);

/** Represent a subrectangle of a ypic as a ypic.
 * Unlike yp_line, this does do bounds-checking and clipping.  This
 * function is the reason `stride` and `w` are separate in ypic:
 * a 10×10 yp_sub of a 1024×1024 canvas or framebuffer still has
 * a stride of 1024.
 */
ypic yp_sub(ypic base, yp_p2 start, yp_p2 size);

/** Copy (possibly overlapping) image rectangles.
 */
void yp_copy(ypic dest, ypic src);

/** Fill an image rectangle.
 */

static inline void
yp_fill(ypic pic, ypix color)
{
  for (int i = 0; i < pic.size.y; i++) {
    ypix *p = yp_line(pic, i);
    for (int j = 0; j < pic.size.x; j++) p[j] = color;
  }
}

/* These three functions read image files, nonincrementally. */
ypic yp_read_png(char *filename);  /* from png.c */
ypic yp_read_jpeg(char *filename); /* from jpeg.c */
ypic yp_read_ppmp6(char *filename); /* from ppmp6-read.c */
/* This function writes one. */
void yp_write_ppmp6(ypic pic, char *filename); /* from ppmp6-write.c */


typedef struct _ywin *ywin;

/** Call yw_open to connect to the X11 display, open a window, and
 * set up a framebuffer for it.  This function takes care of
 * allocating and initializing the data structure.  The options string
 * is kind of a gateway for future options.  Passing an empty string
 * is safe.
 */
ywin yw_open(const char *name, yp_p2 size, const char *options);

/** Call yw_close to disconnect if the process is going to continue.
 */
void yw_close(ywin w);


/** Call yw_frame to get a framebuffer ypic to paint in.
 * The width and height may be different from what you requested in
 * `yw_open` because the user may have resized the window, or because
 * you’re running on the raw hardware framebuffer and therefore you
 * got the whole screen.)
 */
ypic yw_frame(ywin w);

/** Call yw_flip to make visible on the screen the image you
 * have drawn in the framebuffer.
 */
void yw_flip(ywin w);


/** The opaque `yw_event` type returned by `yw_get_event` is
 * polymorphic; it can contain one of `yw_mouse_event`,
 * `yw_key_event`, `yw_die_event`, or `yw_raw_event`.  Call the associated
 * `yw_as_` function for each event type you want to handle.  It
 * will return NULL if the event is of the wrong type.
 *
 * Currently these event types are mutually exclusive, but they offer
 * the possibility of adding event subtypes at some point — either to
 * add more information, such as timestamps or keycodes, or to provide
 * subsets of existing event types, such as a hypothetical
 * `yw_as_keydown_event`.
 */
typedef struct yw_event yw_event;

/** Call `yw_get_event` to get the next input event, or NULL if there
 * is none yet.  The pointer this returns is to the `yw_event` allocated
 * within the ywin struct, so it gets overwritten the next time you
 * call this function.  This function does not block!
 */
yw_event *yw_get_event(ywin w);

/** Call yw_wait to block the program until an input event arrives, or
 * if a nonzero timeout is specified, for that number of
 * microseconds if no event appears.
 */
void yw_wait(ywin w, long timeout_usecs);

typedef struct {
  yp_p2 p;              /* Mouse coordinates relative to upper left */
  u8 buttons;           /* Bitmask of buttons that are down */
} yw_mouse_event;

yw_mouse_event *yw_as_mouse_event(yw_event *);

typedef struct {
  char *text;                 /* pointer to UTF-8 bytes generated */
  u32 keysym;                 /* X11 standard (?) keysym */
  u8 down;                    /* 0 if this is a keyup, 1 if keydown */
  u8 len;                     /* number of bytes in string s */
} yw_key_event;

yw_key_event *yw_as_key_event(yw_event *);

/* useful keysyms */
enum { yk_left = 0xff51, yk_up, yk_right, yk_down,
       yk_pgup, yk_pgdn, yk_end, yk_home,
       yk_shift_l = 0xffe1, yk_shift_r, yk_control_l, yk_control_r,
       yk_caps_lock, yk_shift_lock, yk_meta_l, yk_meta_r,
       yk_backspace = 0xff08, yk_return = 0xff0d };

typedef void *yw_raw_event; /* secretly an XEvent pointer, for the X11 backend */

yw_raw_event *yw_as_raw_event(yw_event *);

typedef int yw_die_event;  /* False unless the event is a die event */

yw_die_event yw_as_die_event(yw_event *);


/** Really primitive font rendering.  I’m not sure if this interface
    is right and will probably change it a lot */
typedef struct yp_font yp_font;
/** Dynamically allocate memory for a font read in from two files. */
yp_font *yp_font_read(char *filename);
/** Find out how much displaying some text in a font would move the “cursor” */
yp_p2 yp_font_escapement(yp_font *font, char *text, int n);
/** Display some text in a font. */
void yp_font_show(yp_font *font, char *text, ypic where);
/** Dispose of the memory for a font. */
void yp_font_delete(yp_font *font);
/** Find the largest character in the font. XXX no allowance for descent */
yp_p2 yp_font_maxsize(yp_font *font);
yp_font *yp_font_scale(yp_font *font, int new_max_height);


/** Call yw_fds to get the Unix file descriptors of the X11
 * connection or other input stream used by a ywin
 * so that you can use it in select() or epoll or whatever.
 * They come in an array of signed ints terminated by a -1 value.
 * Keep in mind that there may already be events pending that have
 * been read from the file descriptor the last time you called
 * yw_wait or yw_get_event.
 */
int *yw_fds(ywin w);

/** If you set yeso_error_handler to point to a function of yours, it
 * will be invoked with a message when there’s an error; the default
 * function prints a message to stderr and calls abort().
 */
typedef void (*yeso_error_handler_t)(const char *msg);
extern yeso_error_handler_t yeso_error_handler;

#undef inline
#undef __attribute__

#endif  /* YESO_H */

#!/usr/bin/luajit
-- Simple Mandelbrot renderer in Lua.
local yeso = require "yeso"

local function mandpix(x, y, palette, lake)
   local zx, zy = x, y   -- z₀ = c
   for _, color in ipairs(palette) do
      if zx*zx + zy*zy > 4 then return color end
      zx, zy = zx*zx - zy*zy + x, 2 * zx*zy + y   -- zᵢ = zᵢ₋₁² + c
   end
   return lake
end

local function mand4pix(x, y, palette, lake)
   local zx, zy = x, y   -- z₀ = c
   for _, color in ipairs(palette) do
      if zx*zx + zy*zy > 4 then return color end
      zx, zy = zx*zx - zy*zy,     2 * zx*zy     -- zᵢ₋₁²
      zx, zy = zx*zx - zy*zy + x, 2 * zx*zy + y -- zᵢ = zᵢ₋₁⁴ + c
   end
   return lake
end

local function mand3pix(x, y, palette, lake)
   local zx, zy = x, y   -- z₀ = c
   for _, color in ipairs(palette) do
      if zx*zx + zy*zy > 4 then return color end
      -- (x + iy)² = x² - y² + 2ixy
      -- (x + iy)³ = (x² - y² + 2ixy)(x + iy) = x³ - y²x + 2ix²y + ix²y - iy³ - 2xy²
      --           = x³ - 3xy² + i(3x²y - y³)
      zx, zy = zx^3 - 3*zx*zy^2 + x, 3*zx^2*zy - zy^3 + y   -- zᵢ = zᵢ₋₁³ + c
   end
   return lake
end

local function mand(fb, pixfunc, cenx, ceny, nscale, maxiter)
   local min = fb.size.x
   if fb.size.y < min then min = fb.size.y end
   local scale = nscale / min
   local cx = fb.size.x / 2
   local cy = fb.size.y / 2
   local palette = {1}
   for i = 1, maxiter do
      table.insert(palette, bit.band(0xffffff, palette[#palette] * 191))
   end

   for y, p in pairs(fb) do
      for x = 0, fb.size.x - 1 do
         p[x] = pixfunc(scale * (x-cx) + cenx,
                        scale * (y-cy) + ceny,
                        palette, 0x00007f)
      end
   end
end

yeso.Window('Mandelbrowser', {1440, 768}):run(function (w)
      local k = yeso.key
      local pixfunc, cx, cy, scale, maxiter = mandpix, 0, 0, 2, 32
      for t = 1, math.huge do
         w:frame(function(fb) mand(fb, pixfunc, cx, cy, scale, maxiter) end)
         w:wait()

         for ev in w:events() do
            if ev.isKey and ev.down ~= 0 then
               local s = ev:s()

               if ev.keysym == k.left then s = 'h' end
               if ev.keysym == k.right then s = 'l' end
               if ev.keysym == k.up then s = 'k' end
               if ev.keysym == k.down then s = 'j' end

               if ev.keysym == k.pgdn then maxiter = maxiter + 32 end
               if ev.keysym == k.pgup and maxiter > 32 then
                  maxiter = maxiter - 32
               end

               if s == '=' or s == '+' then scale = scale / 2 end
               if s == '-' then scale = scale * 2 end
               if s == 'h' or s == 'H' then cx = cx - scale / 4 end
               if s == 'l' or s == 'L' then cx = cx + scale / 4 end
               if s == 'j' or s == 'J' then cy = cy + scale / 4 end
               if s == 'k' or s == 'K' then cy = cy - scale / 4 end
               if s == '1' then pixfunc = mandpix end
               if s == '2' then pixfunc = mand3pix end
               if s == '3' then pixfunc = mand4pix end
            end
         end
      end
end)

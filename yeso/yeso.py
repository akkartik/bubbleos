# -*- coding: utf-8 -*-
"""Partial Python interface to Yeso using ctypes.

This file has both a direct mapping of the C interface (ywin, ypix,
ypic, yp_p2, yw_open, yw_close, yw_frame, yw_flip, yp_line,
yw_get_event, yw_wait, yw_as_key_event, yw_as_mouse_event,
yw_as_die_event, yp_copy, yp_read_png)
and a more Pythonic interface using Window and Ypic objects.

Still missing is error handling, some ypic handling, some image file
access, and yw_fds.

See README.md for documentation of these functions from a C
perspective and yesomunch.py, evtest.py, yesomunchnum.py, and yv.py
for Python examples.

"""

from ctypes import (
    POINTER,
    Structure,
    cast,
    c_char,
    c_char_p,
    c_void_p,
    cdll,
    c_int,
    c_long,
    create_string_buffer,
    c_uint8,
    c_uint32,
    c_int32,
)
import os


# Try Linux framebuffer console interface if not running under X11:
_libname = 'libyeso-xlib.so' if os.getenv('DISPLAY') else 'libyeso-fb.so'

so = cdll.LoadLibrary(os.path.join(os.path.dirname(__file__) or '.',
                                   'so', _libname))


u8 = c_uint8
u32 = c_uint32
ywin = c_void_p

class yp_p2(Structure):
    _fields_ = [
        ('x', c_int32),
        ('y', c_int32),
    ]

    def __iter__(self):
        return iter([self.x, self.y])

ypix = c_uint32

class ypic(Structure):
    _fields_ = [
        ('p', POINTER(ypix)),
        ('size', yp_p2),
        ('stride', c_int),
    ]


yw_open = so.yw_open
yw_open.argtypes = [c_char_p, yp_p2, c_char_p]
yw_open.restype = ywin

yw_close = so.yw_close
yw_close.argtypes = [ywin]

yw_frame = so.yw_frame
yw_frame.argtypes = [ywin]
yw_frame.restype = ypic

yw_flip = so.yw_flip
yw_flip.argtypes = [ywin]

yw_wait = so.yw_wait
yw_wait.argtypes = [ywin, c_long]

yw_event = c_void_p
yw_get_event = so.yw_get_event
yw_get_event.argtypes = [ywin]
yw_get_event.restype = yw_event


class yw_mouse_event(Structure):
    _fields_ = [
        ('p', yp_p2),
        ('buttons', u8),
    ]

yw_as_mouse_event = so.yw_as_mouse_event
yw_as_mouse_event.argtypes = [yw_event]
yw_as_mouse_event.restype = POINTER(yw_mouse_event)


class yw_key_event(Structure):
    _fields_ = [
        ('text', POINTER(c_char)),
        ('keysym', u32),
        ('down', u8),
        ('len', u8),
    ]

yw_as_key_event = so.yw_as_key_event
yw_as_key_event.argtypes = [yw_event]
yw_as_key_event.restype = POINTER(yw_key_event)

yw_die_event = POINTER(c_int)

yw_as_die_event = so.yw_as_die_event
yw_as_die_event.argtypes = [yw_event]
yw_as_die_event.restype = POINTER(yw_die_event)

yp_read_png = so.yp_read_png
yp_read_png.argtypes = [c_char_p]
yp_read_png.restype = ypic

yp_copy = so.yp_copy
yp_copy.argtypes = [ypic, ypic]

# I finally figured out how to do pointer arithmetic with ctypes!
def yp_line(c, y):
    vp = cast(c.p, c_void_p).value
    ap = c_void_p(vp + 4 * y * c.stride)
    return cast(ap, POINTER(ypix))


class Window:
    def __init__(self, title, size, options=""):
        title = title.encode('utf-8')
        options = options.encode('utf-8')

        self.ywin = yw_open(title, yp_p2(*size), options)

    def __enter__(self):
        return self

    def __exit__(self, *exc_info):
        self.close()

    def close(self):
        if self.ywin:
            yw_close(self.ywin)
            self.ywin = None

    def __del__(self):
        # Hopefully you aren’t building your Window objects into a
        # reference cycle!  If not, this method will close windows
        # when they get garbage-collected.  (But you should really use
        # `with`.)
        self.close()

    def frame(self):
        return Frame(self, Ypic(yw_frame(self.ywin)))

    def pending_events(self):
        while True:
            event = yw_get_event(self.ywin)
            if event is None:
                return
            trev = translate_event(event)
            if trev is not None:
                yield trev

    def wait(self, timeout_seconds=None):
        if timeout_seconds == 0:
            return
        if timeout_seconds is None:
            timeout_seconds = 0

        yw_wait(self.ywin, int(round(timeout_seconds * 1e6)))


def translate_event(event):
    if event is None:
        return None
    mev = yw_as_mouse_event(event)
    if mev:
        return MouseEvent(event, mev[0])
    kev = yw_as_key_event(event)
    if kev:
        return KeyEvent(event, kev[0])
    # Don’t ask if it’s a die event unless the client asks us, because
    # asking has side effects.
    return MiscEvent(event)


class BaseEvent:
    is_mouse = False
    is_key = False

    @property
    def is_die(self):
        return bool(yw_as_die_event(self.c_yw_event))

class MouseEvent(BaseEvent):
    is_mouse = True

    def __init__(self, c_yw_event, mev):
        self.c_yw_event = c_yw_event
        self.c_event = mev
        self.p = mev.p
        # Usually what you will do with buttons is test to see whether
        # a button is in it.  For a set of zero to four things, this
        # is just as fast with a list as with a set(), and debugging
        # output is better.
        self.buttons = [i+1 for i in range(8) if (1 << i) & mev.buttons]

class KeyEvent(BaseEvent):
    is_key = True

    def __init__(self, c_yw_event, kev):
        self.c_yw_event = c_yw_event
        self.c_event = kev
        self.text = b''.join(kev.text[i] for i in range(kev.len)).decode('utf-8')
        self.down = (kev.down != 0)
        self.keysym = kev.keysym

class MiscEvent(BaseEvent):
    def __init__(self, c_yw_event):
        self.c_yw_event = c_yw_event


class Frame:
    def __init__(self, window, fb):
        self.window = window
        self.ypic = fb
        self.size = fb.size

    def __enter__(self):
        return self

    def __exit__(self, *exc_info):
        self.close()

    def __del__(self):
        self.close()

    def close(self):
        if self.window:
            yw_flip(self.window.ywin)
            self.ypic = None
            self.window = None

    def line(self):
        return self.ypic.line()

    def __getitem__(self, y):
        return self.ypic[y]

    def __iter__(self):
        return iter(self.ypic)

    def __len__(self):
        return len(self.ypic)

    def asarray(self):
        return self.ypic.asarray()

    def copy(self, source):
        return self.ypic.copy(source)

class Ypic:
    def __init__(self, ypic):
        self.ypic = ypic
        self.size = ypic.size

    def line(self, y):
        return self[y]

    def __getitem__(self, y):
        return yp_line(self.ypic, y)

    def __iter__(self):
        for y in range(self.size.y):
            yield self[y]

    def __len__(self):
        return self.size.y

    def asarray(self):
        "Return the frame pixel buffer as a Numpy array."
        import numpy
        size = self.size.x * self.size.y
        pixels = cast(self.ypic.p, POINTER(ypix * size))[0]
        oned = numpy.frombuffer(pixels, dtype=numpy.uint32)
        return oned.reshape(self.size.y, self.size.x)

    def copy(self, source):
        try:
            source = source.ypic
        except AttributeError:  # maybe it's already a ypic Structure
            pass

        yp_copy(self.ypic, source)


def read_png(filename):
    pic = yp_read_png(filename.encode('utf-8'))
    if not pic.p:
        return None

    return Ypic(pic)

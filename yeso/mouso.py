#!/usr/bin/python3
"""Test app for mouse event handling in yeso.py."""
# -*- coding: utf-8 -*-
import yeso

if __name__ == '__main__':
    with yeso.Window("mouso", (256, 256)) as w:
        drag_start = None
        while True:
            for ev in w.pending_events():
                if ev.is_mouse:
                    mx, my = ev.p
                    if 1 in ev.buttons and drag_start is None:
                        drag_start = ev.p.x, ev.p.y
                    elif 1 not in ev.buttons:
                        drag_start = None

            with w.frame() as f:
                a = f.asarray()
                a[:] = 0x7f7f7f
                if drag_start is not None:
                    dx, dy = drag_start
                    sx = min(dx, mx)
                    ex = max(dx, mx)
                    sy = min(dy, my)
                    ey = max(dy, my)
                    a[sy:ey, sx:ex] = 0x336699

            w.wait()

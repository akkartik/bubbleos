#define _BSD_SOURCE  // for timersub
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>    // XXX POSIX
#include <sys/select.h>  // XXX POSIX
#include <yeso.h>

static inline ypic
block(int x, int y, ypic pic)
{
  return yp_sub(pic, yp_p((x+1) * 20 + 1, y * 20 + 1), yp_p(18, 18));
}

static inline ypic
block_top(int x, int y, ypic pic)
{
  return yp_sub(pic, yp_p((x+1) * 20 + 1, y * 20 + 1), yp_p(3, 18));
}

static inline ypic
block_left(int x, int y, ypic pic)
{
  return yp_sub(pic, yp_p((x+1) * 20 + 1, y * 20 + 1), yp_p(18, 3));
}

static inline ypic
block_right(int x, int y, ypic pic)
{
  return yp_sub(pic, yp_p((x+1) * 20 + 1 + 18-3, y * 20 + 1), yp_p(3, 18));
}

static inline ypic
block_bottom(int x, int y, ypic pic)
{
  return yp_sub(pic, yp_p((x+1) * 20 + 1, y * 20 + 1 + 18-3), yp_p(18, 3));
}

void rotate_piece(char *piece, char piece_rotated[][4], int piece_r)
{
  int origin, xs, ys;
  switch (piece_r) {
  case 0:
    origin = 0;
    xs = 1;
    ys = 4;
    break;
  case 1:
    origin = 3;
    xs = 4;
    ys = -1;
    break;
  case 2:
    origin = 15;
    xs = -1;
    ys = -4;
    break;
  case 3:
  default:
    origin = 12;
    xs = -4;
    ys = 1;
    break;
  }

  for (int y = 0; y != 4; y++) {
    for (int x = 0; x != 4; x++) {
      piece_rotated[x][y] = (' ' != piece[origin + x*xs + y*ys]);
    }
  }
}

int collision(int pit[10][20], char piece_rotated[4][4], int piece_x, int piece_y)
{
  for (int x = 0; x < 4; x++) {
    for (int y = 0; y < 4; y++) {
      if (!piece_rotated[x][y]) continue;
      if (piece_y + y == 20 || piece_y + y < 0) return 1;
      if (piece_x + x < 0 || piece_x + x == 10) return 1;
      if (pit[x + piece_x][y + piece_y]) return 1;
    }
  }
  return 0;
}

ypix dim_octet(ypix c, unsigned int scale)
{
  c = c*scale/50;
  if(c > 0xFF) return 0xFF;
  return c;
}

ypix dim_color(ypix c, unsigned int scale)
{
  // scale 0 = black
  // scale 25 = half dimmed
  // scale 50 = white
  // scale 100 = double bright
  //
  return (dim_octet(c&0xFF,scale)|
          (dim_octet((c>>8)&0xFF,scale)<<8)|
          (dim_octet((c>>16)&0xFF,scale)<<16));
}

ypix colors[] = {0x303030, 0xcc99cc, 0xff3333, 0xffccff, 0xff3399, 0x3366ff, 0x33ff33, 0xccffcc};

void fill_tetris_block(int x, int y, ypic board, int c_idx) {
  yp_fill(block(x, y, board), colors[c_idx]);
  if(c_idx) {
    yp_fill(block_left(x, y, board), dim_color(colors[c_idx],100));
    yp_fill(block_top(x, y, board), dim_color(colors[c_idx],100));
    yp_fill(block_right(x, y, board), dim_color(colors[c_idx],35));
    yp_fill(block_bottom(x, y, board), dim_color(colors[c_idx],35));
  }
}

int main()
{
  ywin w = yw_open("Tetris", yp_p(640, 480), "");
  long drop_us = 900*1000;
  int lines_deleted = 0;
  int frames = 0;
  struct timeval last_drop;
  gettimeofday(&last_drop, NULL);
  srand(last_drop.tv_usec + last_drop.tv_sec);
  int pit[10][20] = {0};
  char pieces[7][16] = {
    "    "
    " ## "
    "##  "
    "    ",

    "    "
    "##  "
    " ## "
    "    ",

    "#   "
    "### "
    "    "
    "    ",

    "    "
    "####"
    "    "
    "    ",

    " #  "
    "### "
    "    "
    "    ",

    "  # "
    "### "
    "    "
    "    ",

    "    "
    " ## "
    " ## "
    "    ",
  };

  yp_font *font = yp_font_read("cuerdas-caoticas-1.jpeg");

  int piece = rand() % 7, piece_x = 3, piece_y = 0, piece_r = 0;
  char piece_rotated[4][4] = {{0}}; /* really bool */
            
  int got_event = 1, time_to_wait = 0;
  for (;;) {
    if (time_to_wait) yw_wait(w, time_to_wait);
    rotate_piece(pieces[piece], piece_rotated, piece_r);

    int dropping = 0, screenshotting = 0;
    for (yw_event *ev; (ev = yw_get_event(w));) {
      got_event = 1;
      if (yw_as_die_event(ev)) {
        yw_close(w);
        return 0;
      }
      yw_key_event *key = yw_as_key_event(ev);
      if (key && key->down) {
        switch (key->keysym) {
        case yk_left: case 'a': case 'A':
          if (!collision(pit, piece_rotated, piece_x-1, piece_y)) piece_x--;
          break;
        case yk_right: case 'd': case 'D':
          if (!collision(pit, piece_rotated, piece_x+1, piece_y)) piece_x++;
          break;
        case yk_up: case 'w': case 'W': ;
          int new_rotation = (piece_r + 1) % 4;
          char piece_rotated_more[4][4];
          rotate_piece(pieces[piece], piece_rotated_more, new_rotation);
          if (!collision(pit, piece_rotated_more, piece_x, piece_y)) {
            piece_r = new_rotation;
            rotate_piece(pieces[piece], piece_rotated, piece_r);
          }
          break;
        case yk_down: case 's': case 'S':
          dropping = 1;
          break;
        case 'i': case 'I':
          screenshotting = 1;
          break;
        }
      }
    }

    /* Has enough time passed to drop automatically? */
    struct timeval now;
    gettimeofday(&now, NULL);
    struct timeval diff;
    timersub(&now, &last_drop, &diff);
    if (diff.tv_usec > drop_us) {
      dropping = 1;
      last_drop = now;
    }

    if (dropping) {
      if (collision(pit, piece_rotated, piece_x, piece_y+1)) {
        for (int x = 0; x < 4; x++) {
          for (int y = 0; y < 4; y++) {
            if (piece_rotated[x][y]) pit[x + piece_x][y + piece_y] = piece + 1;
          }
        }
        piece_y = 0;
        piece_x = 3;
        piece = rand() % 7;
        piece_r = 0;
        rotate_piece(pieces[piece], piece_rotated, piece_r);
      } else {
        piece_y++;
      }
    } else if (!got_event) {
      // If we got some input data, but no events, and we have no
      // scheduled drop, just go back to waiting for input data.
      continue;
    }

    for (int y = 20; y >= 0; y--) {
      for (int x = 0; x != 10; x++) {
        if (!pit[x][y]) goto dont_wipe;
      }
      /* wipe a line out */
      lines_deleted++;
      for (int y2 = y; y2 > 0; y2--) {
        for (int x = 0; x != 10; x++) pit[x][y2] = pit[x][y2-1];
      }
      for (int x = 0; x != 10; x++) pit[x][0] = 0;
      drop_us = drop_us / 16 * 15;
      y = 20;

    dont_wipe:
      ;
    }

    if (collision(pit, piece_rotated, piece_x, piece_y)) { /* game over */
      for (int x = 0; x != 10; x++) {
        for (int y = 0; y != 20; y++) pit[x][y] = 0;
      }
      drop_us = 900*1000;
      printf("%d lines\n", lines_deleted);
      lines_deleted = 0;
    }


    ypic fb = yw_frame(w);
    yp_fill(fb, 0x919793);
    ypic board = yp_sub(fb, yp_p((fb.size.x-12*20)/2, 20), yp_p(12*20, 21*20));
    for (int y = 0; y != 20; y++) {
      yp_fill(block(-1, y, board), 0xcccccc);
      for (int x = 0; x != 10; x++) {
        fill_tetris_block(x, y, board, pit[x][y]);
      }
      yp_fill(block(10, y, board), 0xcccccc);
    }

    for (int x = -1; x != 11; x++) yp_fill(block(x, 20, board), 0xcccccc);

    for (int y = 0; y != 4; y++) {
      for (int x = 0; x != 4; x++) {
        if (piece_rotated[x][y]) {
          fill_tetris_block(x + piece_x, y + piece_y, board, piece + 1);
        }
      }
    }

    char lines_buf[15];
    sprintf(lines_buf, "%d", lines_deleted);
    yp_font_show(font, lines_buf, fb);

    if (screenshotting) {
      screenshotting = 0;
      yp_write_ppmp6(fb, "tetris.ppm");
      yp_fill(fb, -1);
    }
    yw_flip(w);
    got_event = 0;
    // This is currently about 255 frames per second on my
    // laptop without the yw_wait below
    frames++;

    /* Before we rendered frames, we handled all pending input events.
       Now let’s wait until there’s possibly a new pending input
       event, or until a drop is due.  This is done at the top of the
       loop instead of the bottom so that `continue` will hit it.
    */
    struct timeval desired_diff = { .tv_sec = 0, .tv_usec = drop_us };
    struct timeval wait_end;
    timeradd(&last_drop, &desired_diff, &wait_end);
    struct timeval timeout;
    timersub(&wait_end, &now, &timeout);
    time_to_wait = timeout.tv_usec + 1;
  }
}

/* Micro terminal emulator, implementing all of the functionality of the ADM3A.
 *
 * It doesn’t yet do scrollback, or resize.  See admu-shell.c for
 * hanging it off a pty.
 */

#include <string.h>

#include "admu.h"

static inline void
admu_clear(admu *t)
{
  memset(t->screenchars, ' ', t->rows*t->cols);
  t->cursor_pos = 0;
}

void
admu_init(admu *t, int cols, int rows, unsigned char *screenchars)
{
  t->rows = rows;
  t->cols = cols;
  t->screenchars = screenchars;
  t->state = admu_ready;
  t->bell = 0;
  admu_clear(t);
}

static inline int
ctrl(int c)
{
  return c & 0x1f;
}

/* Occasionally the ADM-3A is in an unusual state for a
 * multi-character sequence (specifically, CUP, cursor position),
 * which is handled here.
 */
static inline void
admu_handle_seq(admu *t, unsigned char c)
{
  switch(t->state) {
  case admu_esc:
    t->state = (c == '=') ? admu_cup : admu_ready;
    break;
  case admu_cup:
    t->pending_cup = c - ' ';
    t->state = admu_cup2;
    break;
  case admu_cup2:
    t->cursor_pos = t->pending_cup * t->cols + c - ' ';
    if (t->cursor_pos < 0) t->cursor_pos = 0;
    if (t->cursor_pos >= t->rows*t->cols) t->cursor_pos = t->rows*t->cols - 1;
    t->state = admu_ready;
    break;
  default:
    t->state = admu_ready;
    break;
  }
}

static void
scroll_up_one_line(admu *t)
{
  memmove(t->screenchars, t->screenchars + t->cols, (t->rows-1) * t->cols);
  memset(t->screenchars + (t->rows-1) * t->cols, ' ', t->cols);
  t->cursor_pos -= t->cols;
}

void
admu_handle_char(admu *t, unsigned char c)
{
  if (t->state != admu_ready) {
    admu_handle_seq(t, c);
    return;
  }

  if (c == ctrl('H')) { // backspace
    t->cursor_pos--;
  } else if (c == ctrl('L')) { // right
    t->cursor_pos++;
  } else if (c == ctrl('K')) { // up
    t->cursor_pos -= t->cols;
  } else if (c == ctrl('J')) { // LF, down, "\n", newline
    t->cursor_pos += t->cols;
  } else if (c == '\r') { // cr, ctrl('M')
    t->cursor_pos -= t->cursor_pos % t->cols;
  } else if (c == ctrl('^')) { // ^^ is home
    t->cursor_pos = 0;
  } else if (c == '\032') { // ^Z clears screen
    admu_clear(t);
  } else if (c == '\033') { // ESC to start cup (cursor position) sequence
    t->state = admu_esc;
  } else if (c == ctrl('G')) { // bell
    t->bell = 1;
  } else if ((c & 0xc0) == 0x80) { // UTF-8 continuation byte
    // XXX this would probably work better if these weren’t 8-bit
    // chars.  But at least it should give us the right number of
    // characters of width, except of course where there are combining
    // chars.
    t->screenchars[t->cursor_pos] <<= 6;
    t->screenchars[t->cursor_pos] |= c ^ 0x80;
  } else {
    t->screenchars[t->cursor_pos++] = c;
  }
  if (t->cursor_pos < 0) t->cursor_pos = 0;
  
  while (t->cursor_pos >= t->rows * t->cols) scroll_up_one_line(t);
}
